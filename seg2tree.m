function [outfile] = seg2tree(segfile,params)

    lastversion = '20170830';
    
    %% Params
    segchn = params.channel;
    xyp    = params.xyp; % order preference for matching (X or Y)
    xyorder  = params.xyorder; % order preference
    quantify = params.quantify; % 1 or 0
    fact     = params.fact; % border expansion for bkg, if quantify=1
    singlecells = params.singlecells; % 1 or 0, splits linages into single cells
    verbose     = params.verbose; % 1 or 0 
    deltaclus   = params.deltaclus;
    minolap     = params.minolap;
    maxcsum     = params.maxcsum;
    
    currfolder = [fileparts(which('seg2tree')),'/'];
    addpath(genpath([currfolder,'/src/']));
    
    FSEGfiltfolder = [currfolder,'/src/filters/'];
    files = dir([FSEGfiltfolder,'*.mat']);
    filfiles = cell(length(files),1);
    for asd3 = 1:length(files)
        filfiles{asd3} = files(asd3).name;
    end
    
    display([' Loading segmentation file ',segfile]);
    
    loadin = load(segfile);
    
    frameset = loadin.frameset;
    prefix   = loadin.prefix;
    steps    = loadin.steps;
    xys      = loadin.xys;
    its      = loadin.its;
    chns     = loadin.chns;
    date     = getdate();
    inputseg = segfile;
    
    display([' Loading filters from ',filfiles{loadin.FSEGfiltpop}]);
    temp   = load([FSEGfiltfolder,filfiles{loadin.FSEGfiltpop}]);
    filts  = temp.filts;
    angles = temp.angles;
    na     = temp.na;
    
    if(isfield(loadin,'FSEGtable'))
        FSEGtable = loadin.FSEGtable;
    elseif(isfield(loadin,'ptable'))
        FSEGtable = loadin.ptable;
    else
        error('Could not locate segmentation parameters');
    end
    
    [nsteps,nxys,nchns,nits] = size(frameset);
    treeset = cell(nsteps,nxys);
    for st = 1:nsteps
        for xy = 1:nxys
            display(['  Analysing set: Step ',num2str(st),', XY ',xys{xy}]);
            tree = struct;
            
            display(['   Mapping cells frame by frame']);
            tree.ucells = 0;
            cells1 = frameset{st,xy,segchn,1}.cells;
            nc1    = length(cells1);
            frameset{st,xy,segchn,1}.cellsIDs = 1:nc1;
            tree.ucells = nc1;
            tree.cells  = {};
            temp = load(frameset{st,xy,segchn,1}.filepath);
            time = temp.deltat/60;
            for c = 1:nc1
                tree.cells{c} = initcell(c,1,c,0,1,time);
            end
            for it = 1:(nits-1)
                % Previous frame
                cells1  = frameset{st,xy,segchn,it}.cells;
                prevIDs = frameset{st,xy,segchn,it}.cellsIDs;
                nc1     = length(cells1);
                % This frame
                cells2  = frameset{st,xy,segchn,it+1}.cells;                
                nc2 = length(cells2);
                % Easier to add time now
                temp    = load(frameset{st,xy,segchn,it+1}.filepath);
                time    = temp.deltat/60;
                %
                usedIDs = zeros(size(prevIDs));
                newIDs  = zeros(size(1:nc2));
                % Map cells
                [addcells,chicells,newcells] = mapCellFrames(cells1,cells2,params);
                
                % Add cells into tree
                newIDs    = zeros(nc2,1);
                divevents = zeros(size(prevIDs));
                for c2 = 1:size(addcells,1)
                    oc    = addcells(c2,1);
                    ID    = prevIDs(addcells(c2,2));
                    score = addcells(c2,3);
                    newIDs(oc) = ID;
                    % cell, iteration, index, score, time
                    tree.cells{ID} = addframetocell(tree.cells{ID},it+1,oc,score,time);
                end
                for c2 = 1:size(chicells,1)
                    oc    = chicells(c2,1);
                    pID   = prevIDs(chicells(c2,2));
                    score = chicells(c2,3);
                    tree.ucells = tree.ucells+1;
                    ID  = tree.ucells;
                    newIDs(oc) = ID;
                    % ID, iteration, index, parent, score, time
                    tree.cells{ID} = initcell(ID,it+1,oc,pID,score,time);
                    % ID, iteration, child
                    if(divevents(chicells(c2,2))==1)
                        display('Two divisions in frame ',num2str(it));
                    end
                    divevents(chicells(c2,2)) = 1;
                    tree.cells{pID} = adddivision(tree.cells{pID},it+1,ID);
                end
                for c2 = 1:length(newcells)
                    oc = newcells(c2);
                    tree.ucells = tree.ucells+1;
                    ID  = tree.ucells;
                    newIDs(oc) = ID;
                    % ID, iteration, index, parent, score, time
                    tree.cells{ID} = initcell(ID,it+1,oc,0,1,time);
                end
                frameset{st,xy,segchn,it+1}.cellsIDs = newIDs;
                
                % check just in case
                if(~(isempty(find(newIDs==0))&& length(newIDs)==nc2))
                    error('Correct cellIDs on frame');
                end
            end
            
            tree.it2cells = zeros(tree.ucells,nits);
            for c = 1:tree.ucells
                tree.it2cells(c,tree.cells{c}.inframes) = 1;
            end
            
            if(quantify==1) % Add quantifications
                display(['   Adding quantification']);                
                for c = 1:tree.ucells
                    tree.cells{c}.areas   = zeros(size(tree.cells{c}.inframes));
                    tree.cells{c}.lengths = zeros(size(tree.cells{c}.inframes));
                    tree.cells{c}.widths  = zeros(size(tree.cells{c}.inframes));
                    tree.cells{c}.angles  = zeros(size(tree.cells{c}.inframes));
                    tree.cells{c}.center  = zeros(length(tree.cells{c}.inframes),2);
                    tree.cells{c}.fluos   = zeros(length(tree.cells{c}.inframes),nchns);
                    tree.cells{c}.tfluos  = zeros(length(tree.cells{c}.inframes),nchns);
                    tree.cells{c}.bkgs    = zeros(length(tree.cells{c}.inframes),nchns);
                end
                for it = 1:nits                     
                    cells   = frameset{st,xy,segchn,it}.cells;
                    ncells  = length(cells);
                    borders = cell(1,ncells);
                    cIDs    = frameset{st,xy,segchn,it}.cellsIDs;
                    for c = 1:ncells
                        borders{c} = expandborder(cells{c}.center,cells{c}.border,fact);
                        birth = tree.cells{cIDs(c)}.inframes(1);
                        tree.cells{cIDs(c)}.areas(it-birth+1)    = cells{c}.area;
                        tree.cells{cIDs(c)}.lengths(it-birth+1)  = cells{c}.length;
                        tree.cells{cIDs(c)}.widths(it-birth+1)   = cells{c}.width;
                        tree.cells{cIDs(c)}.angles(it-birth+1)   = cells{c}.angle;
                        tree.cells{cIDs(c)}.center(it-birth+1,:) = cells{c}.center;
                    end
                    for ch = 1:nchns
                        imgpath = frameset{st,xy,ch,it}.filepath;
                        temp = load(imgpath);
                        img = double(temp.img);
                        for c = 1:ncells
                            birth = tree.cells{cIDs(c)}.inframes(1);
                            tree.cells{cIDs(c)}.tfluos(it-birth+1,ch) = sum(img(cells{c}.PixelIdxList));
                            tree.cells{cIDs(c)}.fluos(it-birth+1,ch) = mean(img(cells{c}.PixelIdxList));
                            tree.cells{cIDs(c)}.bkgs(it-birth+1,ch)  = mean(readborder(img,borders{c}));
                        end
                    end
                end
            end
            
            if(singlecells) % separate into single trajectories
                display(['   Dividing into single trajectories']);
                count = 0;
                tree.cells2 = {};
                for c1 = 1:tree.ucells
                    if(isempty(tree.cells{c1}.childs)) % No childs, easy
                        count = count+1;
                        newID = count;
                        tree.cells2{newID}     = tree.cells{c1};
                        tree.cells2{newID}.ID2 = newID;
                        tree.cells2{newID}.divisions = 0;
                        tree.cells{c1}.ID2     = newID;
                        if(tree.cells{c1}.parent == 0) 
                            % cell appeared and never divided
                            tree.cells2{newID}.parent2 = 0;
                        end
                    else
                        % separate lineage by divisions
                        init = tree.cells{c1}.inframes(1);
                        divs = tree.cells{c1}.divisions;
                        tree.cells{c1}.ID2 = [];
                        d1 = 1;
                        for n  = 1:[length(divs)+1]
                            if(n==(length(divs)+1))
                                d2 = length(tree.cells{c1}.inframes);
                            else
                                d2 = divs(n)-init+1;
                            end
                            if(d1>d2)
                                tree.cells{c1}
                                error();
                            end
                            count = count+1;
                            newID = count;
                            
                            tree.cells2{newID}     = tree.cells{c1};
                            tree.cells2{newID}.ID2 = newID;
                            tree.cells{c1}.ID2     = [tree.cells{c1}.ID2,newID];
                            
                            tree.cells2{newID}.inframes      = tree.cells{c1}.inframes(d1:d2);
                            tree.cells2{newID}.inframesIndex = tree.cells{c1}.inframesIndex(d1:d2);
                            %tree.cells2{newID}.mapscore      = tree.cells{c1}.mapscore(d1:d2);
                            
                            if(quantify==1)
                                tree.cells2{newID}.times     = tree.cells{c1}.times(d1:d2);
                                tree.cells2{newID}.areas     = tree.cells{c1}.areas(d1:d2);
                                tree.cells2{newID}.lengths   = tree.cells{c1}.lengths(d1:d2);
                                tree.cells2{newID}.widths    = tree.cells{c1}.widths(d1:d2);
                                tree.cells2{newID}.angles    = tree.cells{c1}.angles(d1:d2);
                                tree.cells2{newID}.center    = tree.cells{c1}.center(d1:d2,:);
                                tree.cells2{newID}.fluos     = tree.cells{c1}.fluos(d1:d2,:);
                                tree.cells2{newID}.tfluos    = tree.cells{c1}.tfluos(d1:d2,:);
                                tree.cells2{newID}.bkgs      = tree.cells{c1}.bkgs(d1:d2,:);
                            end
                            d1 = d2+1;
                            if(n>1) % let's take advantage
                                tree.cells2{newID}.parent2 = newID-1;
                            elseif(tree.cells{c1}.parent == 0)
                                % just appeared
                                tree.cells2{newID}.parent2 = 0;
                            end
                            if(n==(length(divs)+1))
                                tree.cells2{newID}.divisions = 0;
                                tree.cells2{newID}.childs    = [];
                            else
                                tree.cells2{newID}.divisions = 1;
                                tree.cells2{newID}.childs    = [count+1];
                            end
                        end
                    end
                end
                tree.ucells2 = count;
                % correct parents and childs
                for c1 = 1:tree.ucells
                    ID2s = tree.cells{c1}.ID2;
                    if(length(tree.cells{c1}.ID2)>1)
                        % Add parents from other branch childs
                        childs1 = tree.cells{c1}.childs;
                        for n = 1:length(childs1)
                            child1    = childs1(n);
                            child1ID2 = tree.cells{child1}.ID2(1);
                            par       = ID2s(n);
                            if(isempty(child1ID2) || isempty(par) || ...
                               child1ID2<=0 || par<=0)
                                error();
                            end
                            tree.cells2{child1ID2}.parent2 = par;
                            tree.cells2{par}.childs = [tree.cells2{par}.childs,child1ID2];
                            if(tree.cells2{par}.inframes(end)==(tree.cells2{child1ID2}.inframes(1)-1))
                                % check that the end and start match
                            else
% $$$                                 tree.cells2{par}
% $$$                                 tree.cells2{par}.inframes
% $$$                                 tree.cells2{child1ID2}
% $$$                                 tree.cells2{child1ID2}.inframes
                                display('Inconsistency while mapping parents');
                            end
                        end
                    end
                end
                % let's check none is missing
                for c2 = 1:tree.ucells2
                    if(~(isempty(tree.cells2{c2}.childs)||length(tree.cells2{c2}.childs==2)))
                        tree.cells2{c2}
                        error('Wrong number of childs');
                    end
                    if(~isfield(tree.cells2{c2},'parent2'))
                        tree.cells2{c2}
                        error('While making single cells parent is missing');
                    end
                end
            end
            
            treeset{st,xy} = tree;
        end
    end
    outfile = [segfile(1:end-4),'_tree.mat'];
    display(['  Saving result in ',outfile]);
    save(outfile,...
         'lastversion','frameset','treeset',...
         'prefix','steps','xys','its',...
         'chns','date','inputseg');

end

function [st] = initcell(ID,it,c,p,s,t)
    st = struct;
    st.ID = ID;
    st.parent = p;
    st.childs = [];
    st.inframes = [it];
    st.times    = [t];
    st.inframesIndex = [c];
    st.mapscore = [s];
    st.divisions = [];
end

function [cell] = addframetocell(cell,it,c,s,t)
    cell.inframes = [cell.inframes,it];
    cell.times    = [cell.times,t];
    cell.inframesIndex = [cell.inframesIndex,c];
    cell.mapscore = [cell.mapscore,s];
end

function [cell] = adddivision(cell,it,child)
    cell.childs = [cell.childs,child];
    cell.divisions = [cell.divisions,it-1];
end

% Let's move this away for now
% $$$                 % Try to split cells that were mapped to more than 1
% $$$                 % Assuming that is the main problem
% $$$                 img2    = double(temp.img);
% $$$                 if(sum(problemcells)>0)
% $$$                     [~,probind] = find(problemcells==1);
% $$$                     % Calculate FSEG score
% $$$                     img2    = imfilter(img2,fspecial('gaussian',FSEGtable{3,2},FSEGtable{4,2}),'replicate');
% $$$                     [score] = getFSEGscore(img2,filts,angles,na);
% $$$                     % Go one by one
% $$$                     for pc = 1:length(probind)
% $$$                         c = probind(pc);
% $$$ % $$$                         display(['   In iteration ',num2str(it+1),' : trying to edit cell ',num2str(c),' -- it has \# candidates ',num2str(size(candidates{c},1))]);
% $$$                         cstruct = cells2{c};
% $$$                         % Try to iterative remove pixels until you divide
% $$$                         csplit  = 1;
% $$$                         rmvpix  = 0;
% $$$                         pixs    = cstruct.PixelIdxList;
% $$$                         [~,pixsinds] = sort(score(pixs),'ascend');
% $$$                         for rmvpix = 2:floor(length(pixsinds)*maxremove)
% $$$                             % don't do more than a fraction given by maxremove
% $$$                             newpixs  = pixs(pixsinds(rmvpix:end));
% $$$                             mask     = zeros(size(score));
% $$$                             mask(newpixs) = 1;
% $$$                             newcells = mask2cells(mask);
% $$$                             csplit   = length(newcells);
% $$$                             if(csplit>1)
% $$$                                 break;
% $$$                             end
% $$$                         end
% $$$                         if(csplit==1)
% $$$ % $$$                             display('    Could not split the cell');
% $$$                         elseif(csplit>2)
% $$$                             error('Cell split in more than 2. Aborting');
% $$$                         else
% $$$ % $$$                             display(['    ',num2str(rmvpix),' pixels were removed (',num2str(100*rmvpix/length(pixsinds)),'\%)']);
% $$$                             cells2{c}     = newcells{1};
% $$$                             cells2{nc2+1} = newcells{2};
% $$$                             nc2 = nc2+1;
% $$$                             % Go back to candidates
% $$$                             % Pick the best one
% $$$                             distcands = candidates{c};
% $$$                             for nc = [c,nc2]
% $$$                                 candsai = zeros(size(distcands,1),1);
% $$$                                 for c1 = 1:length(distcands)
% $$$                                     candsai(c1) = areaintersect(cells2{nc},cells1{distcands(c1)});
% $$$                                 end
% $$$                                 [~,aicut] = max(candsai); % get the maximum
% $$$                                 candidates{nc} = [distcands(aicut)',candsai(aicut)'];
% $$$                             end
% $$$                         end
% $$$                     end
% $$$                     % update cells2
% $$$                     frameset{st,xy,segchn,it+1}.cells = cells2;                
% $$$                     [pos1,pos2,dmat,imat] = celldistancematrix(cells1,cells2);
% $$$                 end
