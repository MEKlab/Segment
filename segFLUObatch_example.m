
foldername = '~/phd_microscopy/image/201706_cipro/20170612_e302_gly_cipro0_0004/';
segchn = 3; % mcherry
paramsfile = './parameters/20170612_MACS_Gly.mat';
filtfile   = './src/filters/NA-9_DM-3.0_SG-0.50.mat';
segFLUObatch(foldername,segchn,paramsfile,filtfile);