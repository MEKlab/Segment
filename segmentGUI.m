function [frameset] = segmentGUI()

    lastversion = '20170830';
    
    currfolder = [fileparts(which('segmentGUI')),'/'];
    addpath(genpath([currfolder,'/src/']));
    
    gui = struct;
    gui.myblack = 0.2*[1,1,1];
    gui.mywhite = 0.99*[1,1,1];
    gui.defbkgcol = [0.94,0.94,0.94];
    gui.busycolor = 'yellow';
    gui.OFFcolor  = 'red';
    gui.ONcolor   = 'green';
    gui.FSEGfiltfolder = [currfolder,'/src/filters/'];
    
    gui.handleMap = struct;
    gui.folder    = '';
    gui.date = getdate();
    
    gui.figX = 1300;
    gui.figY = 720;
     
    Xoffset  = 0.005*gui.figX;
    Yoffset  = 0.005*gui.figY;
    dw1      = 5;
    bheight1 = 40;
    bwidth1   = 91;
    
    frameset = {};

    lastc = 0;
    % https://uk.mathworks.com/matlabcentral/answers/96816-how-do-i-change-the-default-background-color-of-all-figure-objects-created-in-matlab
    prevcolor = get(0,'defaultfigurecolor');
    set(0,'defaultfigurecolor',gui.myblack); %
    fig = figure('Visible','off',...
                 'MenuBar','none',...
                 'ToolBar','figure',...
                 'Color',gui.myblack,...
                 'KeyPressFcn',@segmentGUI_KeyPressFcn,...
                 'Position',[200,200,gui.figX,gui.figY]);
    set(0,'defaultfigurecolor',prevcolor);
    %get(fig)
    % custom pointer? https://uk.mathworks.com/help/matlab/ref/figure-properties.html#zmw57dd0e277372
    
    p1dx = 0.28-0.005;
    p1dy = 1-0.005;
    
    %% TABS 
    gui.tabgp(1) = uitabgroup(fig,'Position',[0.005,0.005,1-p1dx-0.24,p1dy]);
    gui.tabs(1) = uitab(gui.tabgp(1),'Title','Viewer');    
    
    gui.tabgp(2) = uitabgroup(fig,'Position',[1-p1dx-0.24+0.005,0.005,0.5,0.3]);
    gui.tabs(2) = uitab(gui.tabgp(2),'Title','Main');
    
    gui.tabgp(3) = uitabgroup(fig,'Position',[1-p1dx-0.24+0.005,0.31+0.005,0.5,0.69]);
    gui.tabs(3) = uitab(gui.tabgp(3),'Title','Fluo  Segment');
    gui.tabs(4) = uitab(gui.tabgp(3),'Title','Phase Segment');
    gui.tabs(5) = uitab(gui.tabgp(3),'Title','Fluo  Spots');
    gui.tabs(6) = uitab(gui.tabgp(3),'Title','MM Segment');
    
    gui.tabs(7) = uitab(gui.tabgp(1),'Title','Merge');    
    gui.tabs(8) = uitab(gui.tabgp(3),'Title','Bfield Segment');
    
    %% Main Microscope-Control
    gui.panels(1) = uipanel(gui.tabs(2),'Title','Main','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,0.5]);
    gui.panels(2) = uipanel(gui.tabs(2),'Title','Navigation','FontSize',12,...
                            'Position',[0.005,0.5+0.005,1-0.005,0.5]);
    
    %% Microscope-Control View Panels    
    gui.panels(3) = uipanel(gui.tabs(1),'Title','Image','FontSize',12,...
                            'Position',[0.005,0.15+0.005,1-0.005,0.85-0.005]);    
    
    %% FSEG Panels
    gui.panels(4) = uipanel(gui.tabs(3),'Title','Parameters','FontSize',12,...
                            'Position',[0.5+0.005,0.5+0.005,0.5-0.005,0.5-0.005]);
    gui.panels(5) = uipanel(gui.tabs(3),'Title','Run','FontSize',12,...
                            'Position',[0.5+0.005,0.005,0.5-0.005,0.5-0.005]);  
    gui.panels(6) = uipanel(gui.tabs(3),'Title','Refine','FontSize',12,...
                            'Position',[0.005,0+0.005,0.5-0.005,1-0.005]);  
    %% PSEG Panels
    gui.panels(7) = uipanel(gui.tabs(4),'Title','Parameters','FontSize',12,...
                            'Position',[0.5+0.005,0.5+0.005,0.5-0.005,0.5-0.005]);
    gui.panels(8) = uipanel(gui.tabs(4),'Title','Run','FontSize',12,...
                            'Position',[0.5+0.005,0.005,0.5-0.005,0.5-0.005]);  
    gui.panels(9) = uipanel(gui.tabs(4),'Title','Refine','FontSize',12,...
                            'Position',[0.005,0+0.005,0.5-0.005,1-0.005]);  
    
    %% FSPO Panels
    gui.panels(10) = uipanel(gui.tabs(5),'Title','Parameters','FontSize',12,...
                            'Position',[0.5+0.005,0.5+0.005,0.5-0.005,0.5-0.005]);
    gui.panels(11) = uipanel(gui.tabs(5),'Title','Run','FontSize',12,...
                            'Position',[0.5+0.005,0.005,0.5-0.005,0.5-0.005]);  
    gui.panels(12) = uipanel(gui.tabs(5),'Title','Refine','FontSize',12,...
                            'Position',[0.005,0+0.005,0.5-0.005,1-0.005]);  
    
    %% MSEG Panels
    gui.panels(13) = uipanel(gui.tabs(6),'Title','Parameters','FontSize',12,...
                            'Position',[0.5+0.005,0.3+0.005,0.5-0.005,0.7-0.005]);
    gui.panels(14) = uipanel(gui.tabs(6),'Title','Run','FontSize',12,...
                           'Position',[0.5+0.005,0.005,0.5-0.005,0.3-0.005]);  
    gui.panels(15) = uipanel(gui.tabs(6),'Title','Refine','FontSize',12,...
                            'Position',[0.005,0+0.005,0.5-0.005,1-0.005]);  
    
    %% Microscope-Control View Panels    
    gui.panels(16) = uipanel(gui.tabs(7),'Title','Merged view','FontSize',12,...
                             'Position',[0.005,0.15+0.005,1-0.005,0.85-0.005]);    
    gui.panels(17) = uipanel(gui.tabs(7),'Title','Options','FontSize',12,...
                             'Position',[0.005,0.005,1-0.005,0.15-0.005]); 
    
    %% BSEG Panels
    gui.panels(18) = uipanel(gui.tabs(8),'Title','Parameters','FontSize',12,...
                            'Position',[0.5+0.005,0.3+0.005,0.5-0.005,0.7-0.005]);
    gui.panels(19) = uipanel(gui.tabs(8),'Title','Run','FontSize',12,...
                           'Position',[0.5+0.005,0.005,0.5-0.005,0.3-0.005]);  
    gui.panels(20) = uipanel(gui.tabs(8),'Title','Refine','FontSize',12,...
                            'Position',[0.005,0+0.005,0.5-0.005,1-0.005]); 

    %% Mains
    labelstrs = {'Open folder','Open set','Save','Quit'};
    for i = [1:4]
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1]);
    end
    set(gui.controls(1+lastc),'Callback',@HloadFolderbutton_Callback);
    set(gui.controls(2+lastc),'Callback',@HloadSetbutton_Callback);
    set(gui.controls(3+lastc),'Callback',@Hsavebutton_Callback);
    set(gui.controls(4+lastc),'Callback',@Hquitbutton_Callback);
    lastc = 4+lastc;
    
    %% ,'MEK-Metamorph','DL-Metamorph','MEK-Live'
    labelstrs = {{'Microscope-Control'},...
                 {'Max project','Mean project','Sum project',...
                  'Fixed Stack','Max gradient'}};
    for i = 1:2
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','popup','String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2*bwidth1),3*dw1+bheight1,2*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.fileModepop = 1+lastc;
    gui.handleMap.projModepop = 2+lastc;
    set(gui.controls(2+lastc),'Value',3);
    set(gui.controls(2+lastc),'Callback',@updateIMGproj_Callback);
    %set(gui.controls(i+2),'Callback',@updateIMGproj_Callback);
    lastc = 2+lastc;
    
    gui.controls(1+lastc) = uicontrol(gui.panels(1),'Style','text',...
                                      'FontSize',12,...
                                      'String','Rotate',...
                                      'Position',[dw1+(5-1)*(dw1+bwidth1),dw1+bheight1,bwidth1,bheight1]);
    gui.controls(2+lastc) = uicontrol(gui.panels(1),'Style','edit',...
                                      'FontSize',12,...
                                      'String','0',...
                                      'Position',[dw1+(6-1)*(dw1+bwidth1),2*dw1+bheight1,bwidth1,bheight1]);
    set(gui.controls(2+lastc),'Callback',@updateIMGproj_Callback);
    gui.handleMap.rotateIMG = 2+lastc;
    lastc = 2+lastc;
    gui.controls(1+lastc) = uicontrol(gui.panels(1),'Style','text',...
                                      'FontSize',12,...
                                      'String','Stack #',...
                                      'Position',[dw1+(5-1)*(dw1+bwidth1),dw1,bwidth1,bheight1]);
    gui.controls(2+lastc) = uicontrol(gui.panels(1),'Style','edit',...
                                      'FontSize',12,...
                                      'String','1',...
                                      'Position',[dw1+(6-1)*(dw1+bwidth1),2*dw1,bwidth1,bheight1]);
    set(gui.controls(2+lastc),'Callback',@updateIMGproj_Callback);
    gui.handleMap.choosenstack = 2+lastc;
    lastc = 2+lastc;
    
    %% Microscope-Control UIcontrol
    gui.controls(1+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','Load a folder with data',...
                                      'HorizontalAlignment','center',...
                                      'FontSize',14,...
                                      'Position',[dw1,bheight1-5,4*bwidth1,bheight1]);
    
    gui.controls(2+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','CHN',...
                                      'HorizontalAlignment','left',...
                                      'FontSize',12,...
                                      'Position',[(3-1)*(dw1+2.2*bwidth1)-10,bheight1-5,40,bheight1*0.75]);
    gui.controls(3+lastc) = uicontrol(gui.panels(2),'Style','popup','String',{''},...
                                      'Callback',@updateIMGpop_Callback,...
                                      'Position',[25+dw1+(3-1)*(dw1+2.2*bwidth1),bheight1,1.9*bwidth1,bheight1*0.75]);
    gui.handleMap.('PrefixTitle') = 1+lastc;
    gui.handleMap.('CHpopup')     = 3+lastc;
    lastc = 3+lastc;
    
    labelstrs = {'ST','XY','IT'};
    typestrs = {'text','text','text'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2.2*bwidth1),dw1-5,25,bheight1*0.75]);
    end
    lastc = 3+lastc;
    
    labelstrs = {{''},{''},{''}};
    typestrs = {'popup','popup','popup'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Callback',@updateIMGpop_Callback,...
                                          'Position',[25+dw1+(i-1)*(dw1+2.2*bwidth1),dw1,1.9*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.('STpopup') = 1+lastc;
    gui.handleMap.('XYpopup') = 2+lastc;
    gui.handleMap.('ITpopup') = 3+lastc;
    lastc = 3+lastc;

    %% IMG 
    img = zeros(512,512);
    gui.axes(1) = axes('Position',[0,0.05,0.95,0.95],'Parent',gui.panels(3));    
    gui.imgs    = imshow(img,[],'Parent',gui.axes(1));
    set(gui.imgs,'ButtonDownFcn',@MouseShowValue);
    gui.handleMap.imageAxes = 1;
    
    gui.controls(1+lastc) = uicontrol(gui.tabs(1),'Style','text',...
                                      'String','Click on image',...
                                      'FontSize',12,...
                                      'Position',[0,75,600,bheight1*0.5]);
    gui.handleMap.imageValue = 1+lastc;
    lastc = 1+lastc;
    
    %% IMG navigation
    labelstrs = {'IT>','IT<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,100+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpIT);
    set(gui.controls(2+lastc),'Callback',@moveDwIT);
    lastc = 2+lastc;
    labelstrs = {'CH>','CH<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,200+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpCH);
    set(gui.controls(2+lastc),'Callback',@moveDwCH);
    lastc = 2+lastc;
    labelstrs = {'XY>','XY<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,300+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpXY);
    set(gui.controls(2+lastc),'Callback',@moveDwXY);
    lastc = 2+lastc;
    
    labelstrs = {'Z>','Z<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,400+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpZ);
    set(gui.controls(2+lastc),'Callback',@moveDwZ);
    lastc = 2+lastc;
    
    %% Fluo Segmentation parameters
    files = dir([gui.FSEGfiltfolder,'*.mat']);
    filfiles = cell(length(files),1);
    for asd3 = 1:length(files)
        filfiles{asd3} = files(asd3).name;
    end
    gui.controls(1+lastc) = uicontrol(gui.panels(4),'Style','popup',...
                                      'FontSize',12,...
                                      'String',filfiles,...
                                      'Value',3,...
                                      'Position',[2*dw1,190+dw1,3.1*bwidth1,bheight1*0.75]);
    gui.handleMap.FSEGfiltpop = 1+lastc;
    
    lastc = lastc+1;
    
    columnNames = {'Name','Value'};
    tableData = {'minfluo',1000;...
                 'minarea',30;...
                 'gfmu',3;...
                 'gfsig',0.5;...
                 'scut',0.92;...
                 'smooth?',0};
    gui.tables(1) = uitable(gui.panels(4),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,bheight1+dw1,3.1*bwidth1,3.7*(bheight1)]);
    gui.handleMap.FSEGtable = 1;
    
    labelstrs = {'Load','Save'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(4),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(2*dw1+1.5*bwidth1),dw1,1.5*bwidth1,bheight1*0.8]);
    end   
    set(gui.controls(1+lastc),'Callback',@LoadParams_FSEG_Callback);
    set(gui.controls(2+lastc),'Callback',@SaveParams_FSEG_Callback);
    lastc = 2+lastc;        
    
    %% Fluo Segmentation Run
    labelstrs = {'Show mask','Show score','Fluo cut'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@FluoMask_Callback);
    set(gui.controls(2+lastc),'Callback',@FluoScore_Callback);
    set(gui.controls(3+lastc),'Callback',@FluoCut_Callback);
    lastc = 3+lastc;
    labelstrs = {'Run All','','Run F'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@FSEGrunAll_Callback);
    set(gui.controls(3+lastc),'Callback',@FSEGrun1_Callback);
    lastc = 3+lastc;

% $$$     labelstrs = {'','','Score 2'};
% $$$     for i = [1:3]
% $$$         gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
% $$$                                           'String',labelstrs{i},...
% $$$                                           'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
% $$$     end
% $$$     set(gui.controls(3+lastc),'Callback',@FluoScore2_Callback);
% $$$     lastc = 3+lastc;
    
    %% Fluo Segmentation Refinement
    gui.controls(1+lastc) = uicontrol(gui.panels(6),'Style','popup',...
                                      'FontSize',12,...
                                      'String',{'Point value'},...
                                      'Value',1,...
                                      'Callback',@MouseActionPopup_Callback,...
                                      'Position',[2*dw1,260+dw1,3.1*bwidth1,bheight1*0.75]);
    lastc = 1+lastc;
    gui.handleMap.FSEGMouseOptions = 1+lastc;
    
    columnNames = {'Name','Value'};
    tableData = {'smooth',5;...
                 'split',0.05;...
                 'split on','score';...
                 'merge',10;...
                 'reshape',1;...
                 'border',1};
    gui.tables(2) = uitable(gui.panels(6),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,110+dw1,3.1*bwidth1,3.5*(bheight1)]);
    gui.handleMap.FSEGreftable = 2;
    
    labelstrs = {'Merge','Split','Remove'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(6),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.FSEGreftable,...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(3+lastc),'Callback',@RemoveSelectedCells_Callback);
    set(gui.controls(1+lastc),'Callback',@MergeSelectedCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SplitSelectedCells_Callback);
    gui.handleMap.FSEGcellrmvcontrol = 3+lastc;
    gui.handleMap.FSEGmergeitcontrol = 2+lastc;
    lastc = 3+lastc;
    labelstrs = {'Border rmv','Reshape','Smooth'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(6),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.FSEGreftable,...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@RemoveBorderCell_Callback);
    set(gui.controls(2+lastc),'Callback',@ReshapeSelectedCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SmoothSelectedCells_Callback);    
    gui.handleMap.FSEGborderrmvcontrol = 1+lastc;
    gui.handleMap.FSEGsmoothcelcontrol = 3+lastc;
    lastc = 3+lastc;
    labelstrs = {'Blink','Select None','Select All'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(6),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.FSEGreftable,...
                                          'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BlinkCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SelectedNoneCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SelectedAllCells_Callback); 
    lastc = 3+lastc;
    
    %% Phase Segmentation parameters
    files = dir([gui.FSEGfiltfolder,'*.mat']);
    filfiles = cell(length(files),1);
    for asd3 = 1:length(files)
        filfiles{asd3} = files(asd3).name;
    end
    gui.controls(1+lastc) = uicontrol(gui.panels(7),'Style','popup',...
                                      'FontSize',12,...
                                      'String',filfiles,...
                                      'Value',10,...
                                      'Position',[2*dw1,190+dw1,3.1*bwidth1,bheight1*0.75]);
    gui.handleMap.PSEGfiltpop = 1+lastc;
    lastc = lastc+1;
    
    columnNames = {'Name','Value'};
    tableData = {'maxfluo',1200;...
                 'minarea',50;...
                 'gfmu',3;...
                 'gfsig',0.5;...
                 'scut',0.92;...
                 'smooth?',0};
    gui.tables(3) = uitable(gui.panels(7),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,bheight1+dw1,3.1*bwidth1,3.7*(bheight1)]);
    gui.handleMap.PSEGtable = 3;
    
    labelstrs = {'Load','Save'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(7),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(2*dw1+1.5*bwidth1),dw1,1.5*bwidth1,bheight1*0.8]);
    end   
    set(gui.controls(1+lastc),'Callback',@LoadParams_PSEG_Callback);
    set(gui.controls(2+lastc),'Callback',@SaveParams_PSEG_Callback);
    lastc = 2+lastc;

    %% Segmentation Run
    labelstrs = {'Show mask','Show score','Phase cut'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(8),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@PhaseMask_Callback);
    set(gui.controls(2+lastc),'Callback',@PhaseScore_Callback);
    set(gui.controls(3+lastc),'Callback',@PhaseCut_Callback);
    lastc = 3+lastc;
    labelstrs = {'','Run All','Run F'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(8),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(2+lastc),'Callback',@PSEGrunAll_Callback);
    set(gui.controls(3+lastc),'Callback',@PSEGrun1_Callback);
    lastc = 3+lastc;
    
    %% Segmentation refinement
    columnNames = {'Name','Value'};
    tableData = {'smooth',5;...
                 'split',0.05;...
                 'split on','score';...
                 'merge',10;...
                 'reshape',1};
    gui.tables(4) = uitable(gui.panels(9),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,110+dw1,3.1*bwidth1,3.5*(bheight1)]);
    gui.handleMap.PSEGreftable = 4;

    labelstrs = {'Merge','Split','Remove'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(9),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.PSEGreftable,...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(3+lastc),'Callback',@RemoveSelectedCells_Callback);
    set(gui.controls(1+lastc),'Callback',@MergeSelectedCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SplitSelectedCells_Callback);
    lastc = 3+lastc;
    labelstrs = {'Rmv border','Reshape','Smooth'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(9),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.PSEGreftable,...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@RemoveBorderCell_Callback);
    set(gui.controls(2+lastc),'Callback',@ReshapeSelectedCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SmoothSelectedCells_Callback);    
    lastc = 3+lastc;
    labelstrs = {'Blink','Select None','Select All'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(9),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.PSEGreftable,...
                                          'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BlinkCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SelectedNoneCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SelectedAllCells_Callback); 
    lastc = 3+lastc;
    
    %% Spot Detection parameters
    files = dir([gui.FSEGfiltfolder,'*.mat']);
    filfiles = cell(length(files),1);
    for asd3 = 1:length(files)
        filfiles{asd3} = files(asd3).name;
    end
    gui.controls(1+lastc) = uicontrol(gui.panels(10),'Style','popup',...
                                      'FontSize',12,...
                                      'String',filfiles,...
                                      'Value',19,...
                                      'Position',[2*dw1,190+dw1,3.1*bwidth1,bheight1*0.75]);
    gui.handleMap.SPOTfiltpop = 1+lastc;
    lastc = lastc+1;
    
    columnNames = {'Name','Value'};
    tableData = {'minfluo',200;...
                 'minarea',3;...
                 'gfmu',2;...
                 'gfsig',0.7;...
                 'scut',1.1;...
                 'smooth?',0};
    gui.tables(5) = uitable(gui.panels(10),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,bheight1+dw1,3.1*bwidth1,3.7*(bheight1)]);
    gui.handleMap.SPOTtable = 5;
    
    labelstrs = {'Load','Save'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(10),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(2*dw1+1.5*bwidth1),dw1,1.5*bwidth1,bheight1*0.8]);
    end   
    set(gui.controls(1+lastc),'Callback',@LoadParams_SPOT_Callback);
    set(gui.controls(2+lastc),'Callback',@SaveParams_SPOT_Callback);
    lastc = 2+lastc;
    
    %% Spot detection Run
    labelstrs = {'Show mask','Show score','Show cut'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(11),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@SpotMask_Callback);
    set(gui.controls(2+lastc),'Callback',@SpotScore_Callback);
    set(gui.controls(3+lastc),'Callback',@SpotCut_Callback);
    lastc = 3+lastc;
    labelstrs = {'','Run All','Run F'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(11),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(2+lastc),'Callback',@SPOTrunAll_Callback);
    set(gui.controls(3+lastc),'Callback',@SPOTrun1_Callback);
    lastc = 3+lastc;

    %% Spot refinement
    columnNames = {'Name','Value'};
    tableData = {'',0};
    gui.tables(6) = uitable(gui.panels(12),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,110+dw1,3.1*bwidth1,3.5*(bheight1)]);
    gui.handleMap.SPOTreftable = 6;

    labelstrs = {'','','Remove'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(12),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.SPOTreftable,...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(3+lastc),'Callback',@RemoveSelectedCells_Callback);
    %set(gui.controls(1+lastc),'Callback',@MergeSelectedCells_Callback);
    %set(gui.controls(2+lastc),'Callback',@SplitSelectedCells_Callback);
    lastc = 3+lastc;
    labelstrs = {'','',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(12),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.SPOTreftable,...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    %set(gui.controls(2+lastc),'Callback',@ReshapeSelectedCells_Callback);
    %set(gui.controls(3+lastc),'Callback',@SmoothSelectedCells_Callback);    
    lastc = 3+lastc;
    labelstrs = {'Blink','Select None','Select All'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(12),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.SPOTreftable,...
                                          'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BlinkCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SelectedNoneCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SelectedAllCells_Callback); 
    lastc = 3+lastc;

    %% MSEG Segmentation Run
    labelstrs = {'','',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(14),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    %set(gui.controls(1+lastc),'Callback',@FluoMask_Callback);
    %set(gui.controls(2+lastc),'Callback',@FluoScore_Callback);
    %set(gui.controls(3+lastc),'Callback',@FluoCut_Callback);
    lastc = 3+lastc;
    labelstrs = {'','Run All','Run F'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(14),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(2+lastc),'Callback',@MSEGrunAll_Callback);
    set(gui.controls(3+lastc),'Callback',@MSEGrun1_Callback);
    lastc = 3+lastc;
    
    %% MSEG Parameters
    columnNames = {'Name','Value'};
    tableData = {'sig',0.75;...
                 'scmethod',1;...
                 'widref',0.7;...
                 'lcut',0.05;...
                 'minints',450;...
                 'maxpixs',20;...
                 'fitsdist',15;...
                 'deltapix',3;...
                 'addtips',1;...
                 'addextra',1;...
                 'minlength',5;...
                 'helpfig',0;...
                 'rot',0};
    % 
    gui.tables(7) = uitable(gui.panels(13),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,bheight1+dw1,3.1*bwidth1,7*(bheight1)]);
    gui.handleMap.MSEGtable = 7;
    
    labelstrs = {'Load','Save'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(13),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(2*dw1+1.5*bwidth1),dw1,1.5*bwidth1,bheight1*0.8]);
    end   
    set(gui.controls(1+lastc),'Callback',@LoadParams_MSEG_Callback);
    set(gui.controls(2+lastc),'Callback',@SaveParams_MSEG_Callback);
    lastc = 2+lastc;
    
    %% MSEG refine
    labelstrs = {'Merge','','Remove'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(15),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(3+lastc),'Callback',@RemoveSelectedCells_Callback);
    set(gui.controls(1+lastc),'Callback',@MergeSelectedCells_Callback);
    %set(gui.controls(2+lastc),'Callback',@SplitSelectedCells_Callback);
    lastc = 3+lastc;

    labelstrs = {'Blink','Select None','Select All'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(15),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BlinkCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SelectedNoneCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SelectedAllCells_Callback); 
    lastc = 3+lastc;
    
    %% Merge images
    img = zeros(512,512);
    gui.axes(2) = axes('Position',[0,0.01,0.95,0.95],'Parent',gui.panels(16));    
    imshow(img,[],'Parent',gui.axes(2));
    gui.handleMap.mergeImageAxes = 2;
    
    labelstrs = {'IT>','IT<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(16),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[570,100+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@MergemoveUpIT);
    set(gui.controls(2+lastc),'Callback',@MergemoveDwIT);
    lastc = 2+lastc;
% $$$     labelstrs = {'CH>','CH<'};
% $$$     typestrs = {'pushbutton','pushbutton'};
% $$$     for i = [1:2]
% $$$         gui.controls(i+lastc) = uicontrol(gui.panels(16),'Style',typestrs{i},...
% $$$                                           'FontSize',12,...
% $$$                                           'String',labelstrs{i},...
% $$$                                           'Position',[567.5,200+i*(25+5),40,25]);
% $$$     end
% $$$     %set(gui.controls(1+lastc),'Callback',@moveUpCH);
% $$$     %set(gui.controls(2+lastc),'Callback',@moveDwCH);
% $$$     lastc = 2+lastc;
    labelstrs = {'XY>','XY<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(16),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[570,300+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@MergemoveUpXY);
    set(gui.controls(2+lastc),'Callback',@MergemoveDwXY);
    lastc = 2+lastc;

    strs = {'Red','Blue','Green'};
    for i = 1:3
        gui.controls(i+lastc) = uicontrol(gui.panels(17),'Style','text',...
                                          'FontSize',12,...
                                          'String',strs{i},...
                                          'Position',[5,5+(i-1)*(30),70,20]);
    end
    lastc = 3+lastc;
    
    for i = 1:3
        gui.controls(i+lastc) = uicontrol(gui.panels(17),'Style','popup',...
                                          'String',{'none'},...
                                          'Position',[80,12+(i-1)*(30),180,15]);
    end
    gui.handleMap.redpopup    = 1+lastc;
    gui.handleMap.bluepopup  = 2+lastc;
    gui.handleMap.greenpopup  = 3+lastc;
    lastc = 3+lastc;

    columnNames = {'Red','','Blue','','Green',''};
    tableData = {'min',500,'min',500,'min',500;...
                 'max',10000,'max',10000,'max',10000};
    gui.tables(8) = uitable(gui.panels(17),'Data',tableData,...
                            'ColumnName',columnNames,...       
                            'ColumnEditable',[false,true,false,true,false,true],...
                            'Position',[265,5,300,70]);
    set(gui.tables(8),'ColumnWidth',{35,52,35,52,35,52});
    gui.handleMap.MERGEtable = 8;

    gui.controls(1+lastc) = uicontrol(gui.panels(17),'Style','pushbutton',...
                                      'String','Update',...
                                      'Callback',@updateMergeIMG_Callback,...
                                      'Position',[572,5,50,30]);
    gui.controls(2+lastc) = uicontrol(gui.panels(17),'Style','pushbutton',...
                                      'String','Guess',...
                                      'Callback',@GuessMergeTable_Callback,...
                                      'Position',[572,40,50,30]);
    lastc = 2+lastc;
    
    %% BSEG Segmentation Run
    labelstrs = {'Mask','Score','Cut'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(19),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BSEGMask_Callback);
    set(gui.controls(2+lastc),'Callback',@BSEGScore_Callback);
    set(gui.controls(3+lastc),'Callback',@BSEGCut_Callback);
    lastc = 3+lastc;
    labelstrs = {'','Run All','Run F'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(19),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(2+lastc),'Callback',@BSEGrunAll_Callback);
    set(gui.controls(3+lastc),'Callback',@BSEGrun1_Callback);
    lastc = 3+lastc;
    
    %% BSEG Parameters
    files = dir([gui.FSEGfiltfolder,'*.mat']);
    filfiles = cell(length(files),1);
    for asd3 = 1:length(files)
        filfiles{asd3} = files(asd3).name;
    end
    gui.controls(1+lastc) = uicontrol(gui.panels(18),'Style','popup',...
                                      'FontSize',12,...
                                      'String',filfiles,...
                                      'Value',19,...
                                      'Position',[2*dw1,285+dw1,3.1*bwidth1,bheight1*0.75]);
    gui.handleMap.BSEGfiltpop = 1+lastc;
    lastc = lastc+1;
    
    columnNames = {'Name','Value'};
    tableData = {'intcut',5300;...
                 'minarea',50;...
                 'gfmu',3;...
                 'gfsig',0.5;...
                 'scut',-0.04;...
                 'maxarea',10000;...
                 'inblack',1};
    gui.tables(9) = uitable(gui.panels(18),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,bheight1+dw1,3.1*bwidth1,6*(bheight1)]);
    gui.handleMap.BSEGtable = 9;
    
    labelstrs = {'Load','Save'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(18),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(2*dw1+1.5*bwidth1),dw1,1.5*bwidth1,bheight1*0.8]);
    end   
    set(gui.controls(1+lastc),'Callback',@LoadParams_BSEG_Callback);
    set(gui.controls(2+lastc),'Callback',@SaveParams_BSEG_Callback);
    lastc = 2+lastc;
    
    %% BSEG refine
    columnNames = {'Name','Value'};
    tableData = {'smooth',5;...
                 'split',0.05;...
                 'split on','img';...
                 'merge',10;...
                 'reshape',1;...
                 'border',1};
    gui.tables(10) = uitable(gui.panels(20),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[2*dw1,110+dw1,3.1*bwidth1,4*(bheight1)]);
    gui.handleMap.BSEGreftable = 10;
    
    labelstrs = {'Merge','Split','Remove'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(20),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.BSEGreftable,...
                                          'Position',[2*dw1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(3+lastc),'Callback',@RemoveSelectedCells_Callback);
    set(gui.controls(1+lastc),'Callback',@MergeSelectedCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SplitSelectedCells_Callback);
    lastc = 3+lastc;
    
    labelstrs = {'Border rmv','Reshape','Smooth'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(20),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'UserData',gui.handleMap.BSEGreftable,...
                                          'Position',[3*dw1+1*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@RemoveBorderCell_Callback);
    set(gui.controls(2+lastc),'Callback',@ReshapeSelectedCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SmoothSelectedCells_Callback);    
    lastc = 3+lastc;
    
    labelstrs = {'Blink','Select None','Select All'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(20),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[4*dw1+2*bwidth1,dw1+(i-1)*(dw1+bheight1*0.8),1*bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@BlinkCells_Callback);
    set(gui.controls(2+lastc),'Callback',@SelectedNoneCells_Callback);
    set(gui.controls(3+lastc),'Callback',@SelectedAllCells_Callback); 
    lastc = 3+lastc;

    %% Color and display
    for t = 1:length(gui.tabs)
        %set(gui.tabs(t),'ForegroundColor',[0.9,0.4,0]);%[0.6,0,0]
        set(gui.tabs(t),'BackgroundColor',gui.myblack);
    end
    for p = 1:length(gui.panels)
        set(gui.panels(p),'BackgroundColor',gui.myblack);
        % orange
        set(gui.panels(p),'ForegroundColor',[1,0.5,0.25]); %gui.mywhite
    end
    for c = 1:length(gui.controls)
        if(strcmp(get(gui.controls(c),'Style'),'text'))
            set(gui.controls(c),'BackgroundColor',gui.myblack);
            set(gui.controls(c),'ForegroundColor',gui.mywhite);
        else
            set(gui.controls(c),'BackgroundColor',gui.mywhite);
            set(gui.controls(c),'ForegroundColor',gui.myblack);
        end
    end
    
    %% Init
    set([fig,gui.axes,gui.panels,gui.tabs,gui.controls,gui.tables],'Units','normalized');
    set(fig,'Name','Cell Segmentation');
    set(gui.tabgp(3),'SelectedTab',gui.tabs(8)); % bfield
% $$$     set(gui.controls(gui.handleMap.fileModepop),'Value',3); % DL-file mode
    set(gui.controls(gui.handleMap.projModepop),'Value',4); % proj mode
    set(fig,'Visible','on');
    movegui(fig,'center');
    
    function segmentGUI_KeyPressFcn(hObj,event)
        c = event.Key;
        m = event.Modifier;
        if(~strcmp(c,'')) %  && strcmp(m{1},'control')
            switch c
              case 'z' % XY <-
                moveDwXY();
              case 'x' % XY ->
                moveUpXY();
              case 'a' % CH <-
                moveDwCH();
              case 's' % CH ->
                moveUpCH();
              case 'q' % IT <-
                moveDwIT();
              case 'w' % IT ->
                moveUpIT();
              case 'd' % remove cells at border
                RemoveBorderCell_Callback(gui.controls(gui.handleMap.FSEGborderrmvcontrol));
              case 'r' % remove selected cells
                RemoveSelectedCells_Callback(gui.controls(gui.handleMap.FSEGcellrmvcontrol));
              case 'v' % smooth selected cells
                SmoothSelectedCells_Callback(gui.controls(gui.handleMap.FSEGsmoothcelcontrol));
              case 'c' % merge selected cells
                MergeSelectedCells_Callback(gui.controls(gui.handleMap.FSEGmergeitcontrol));
              case 'e' % select none
                [frame,nst,nxy,nch,nit] = getcurrentframe();
                frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frame.selectedcells));
                updateIMGpop_Callback();
              case 'f' % select all
                [frame,nst,nxy,nch,nit] = getcurrentframe();
                frameset{nst,nxy,nch,nit}.selectedcells = zeros(size(frame.selectedcells));
                updateIMGpop_Callback();
                %SelectedAllCells_Callback();
            end
        end
    end
    
    function Hquitbutton_Callback(~,~)
        close(fig);
    end
    
    %% Microscope-control loading
    function [] = HloadFolderbutton_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        foldername = uigetdir();
        if(ischar(foldername))
            foldername = [foldername,'/'];
            if(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==1)
                %% MATLAB files, from Microscope-Control GUI
                imgfiles = dir([foldername,'IMG-*.mat']);
                logfiles = dir([foldername,'*LOG-*.mat']);
                %loadfiles(imgfiles,foldername,2);
                if(length(imgfiles)>0)
                    [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);            
                    gui.folder = foldername;
                    set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
                    if(~isempty(steps))
                        set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
                    end
                    set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);
                    set(gui.controls(gui.handleMap.('ITpopup')),'String',its);
                    set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
                    
                    set(gui.controls(gui.handleMap.('redpopup')),'String',{'none',chns{:}});
                    set(gui.controls(gui.handleMap.('bluepopup')),'String',{'none',chns{:}});
                    set(gui.controls(gui.handleMap.('greenpopup')),'String',{'none',chns{:}});
                    % ST x XY x CHN x IT
                    if(length(steps)==0)
                        nsteps = 1;
                        steps = {''};
                    else
                        nsteps = length(steps);
                    end
                    frameset = cell(nsteps,length(xys),length(chns),length(its));
                    for s = 1:length(steps)
                        for xy = 1:length(xys)
                            for ch = 1:length(chns)
                                for it = 1:length(its)
                                    if(~strcmp(steps{s},''))
                                        filename = ['IMG-',prefix,...
                                                    '_ST-',steps{s},...
                                                    '_XY-',xys{xy},...
                                                    '_CHN-',chns{ch},...
                                                    '_IT-',its{it},...
                                                    '.mat'];
                                    else
                                        filename = ['IMG-',prefix,...
                                                    '_XY-',xys{xy},...
                                                    '_CHN-',chns{ch},...
                                                    '_IT-',its{it},...
                                                    '.mat'];
                                    end
                                    filepath = [gui.folder,filename];
                                    if(~exist(filepath))
                                        error(['File does not exists: ',filepath]);
                                    end
                                    frame = struct;
                                    frame.prefix    = prefix;
                                    frame.step      = steps{s};
                                    frame.xy        = xys{xy};
                                    frame.chn       = chns{ch};
                                    frame.it        = its{it};
                                    frame.folder    = foldername;
                                    frame.filename  = filename;
                                    frame.filepath  = filepath;
                                    frameset{s,xy,ch,it} = frame;
                                end
                            end
                        end            
                    end
                    updateIMGpop_Callback();
                end
            elseif(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==2)
                %% TIF files, from Metamorph MEK
                imgfiles = dir([foldername,'*.TIF']);
                prefix = '';
                steps  = {''};
                xys    = {''};
                chns   = {''};
                its    = {''};
                for n = 1:length(imgfiles)
                    its{n} = num2str(n);
                end
                gui.folder = foldername;
                set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
                set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
                set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);
                set(gui.controls(gui.handleMap.('ITpopup')),'String',its);
                set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
                frameset = cell(1,1,1,length(its));
                for n = 1:length(imgfiles)
                    filename = imgfiles(n).name;
                    frame = struct;
                    frame.prefix    = prefix;
                    frame.step      = steps{1};
                    frame.xy        = xys{1};
                    frame.chn       = chns{1};
                    frame.it        = its{1};
                    frame.folder    = foldername;
                    frame.filename  = filename;
                    frame.filepath  = [foldername,filename];
                    frameset{1,1,1,n} = frame;
                end
                updateIMGpop_Callback();
            elseif(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==3)
                %% TIF files, from Metamorph DL
                imgfiles = dir([foldername,'*.TIF']);
                if(length(imgfiles)>0)
                    [prefix,steps,xys,chns,its] = parseDLfiles(imgfiles);
                    gui.folder = foldername;
                    set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
                    if(~isempty(steps))
                        set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
                    end
                    if(~isempty(xys))
                        set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);
                    end
                    if(~isempty(its))
                        set(gui.controls(gui.handleMap.('ITpopup')),'String',its);
                    end
                    set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
                    set(gui.controls(gui.handleMap.('redpopup')),'String',{'none',chns{:}});
                    set(gui.controls(gui.handleMap.('bluepopup')),'String',{'none',chns{:}});
                    set(gui.controls(gui.handleMap.('greenpopup')),'String',{'none',chns{:}});
                    if(length(steps)==0)
                        nsteps = 1;
                        steps = {''};
                    else
                        nsteps = length(steps);
                    end
                    if(length(xys)==0)
                        nxys = 1;
                        xys  = {''};
                    else
                        nxys = length(xys);
                    end
                    if(length(its)==0)
                        nits = 1;
                        its  = {''};
                    else
                        nits = length(its);
                    end
                    % ST x XY x CHN x IT
                    frameset = cell(nsteps,nxys,length(chns),nits);
                    for s = 1:length(steps)
                        for xy = 1:length(xys)
                            for ch = 1:length(chns)
                                for it = 1:length(its)
                                    % TODO: add stage support!
                                    filename = [prefix,...
                                                '_',xys{xy},...
                                                '_',chns{ch},...
                                                '.TIF'];
                                    filepath = [gui.folder,filename];
                                    if(~exist(filepath))
                                        display(['File does not exists: ',filepath]);
                                        %error(['File does not exists: ',filepath]);
                                    end
                                    frame = struct;
                                    frame.prefix    = prefix;
                                    frame.step      = steps{s};
                                    frame.xy        = xys{xy};
                                    frame.chn       = chns{ch};
                                    frame.it        = its{it};
                                    frame.folder    = foldername;
                                    frame.filename  = filename;
                                    frame.filepath  = filepath;
                                    frameset{s,xy,ch,it} = frame;
                                end
                            end
                        end            
                    end
                    updateIMGpop_Callback();
                end
            else
                error(['Error while parsing folder']);
            end
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
        
    %% TODO: fix saving and loading
    function [] = Hsavebutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        prefix = get(gui.controls(gui.handleMap.('PrefixTitle')),'String');
        [filename,filepath] = uiputfile(['./',prefix,'_segmented.mat'],'Save analysis');
        if(~(filename==0))            
            date   = getdate();
            fileMode    = get(gui.controls(gui.handleMap.fileModepop),'Value');
            projMode    = get(gui.controls(gui.handleMap.projModepop),'Value');
            FSEGtable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
            PSEGtable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
            SPOTtable = get(gui.tables(gui.handleMap.SPOTtable),'Data');
            BSEGtable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
            FSEGfiltpop = get(gui.controls(gui.handleMap.('FSEGfiltpop')),'Value');
            PSEGfiltpop = get(gui.controls(gui.handleMap.('PSEGfiltpop')),'Value');
            SPOTfiltpop = get(gui.controls(gui.handleMap.('SPOTfiltpop')),'Value');
            BSEGfiltpop = get(gui.controls(gui.handleMap.('BSEGfiltpop')),'Value');
            prefix      = get(gui.controls(gui.handleMap.('PrefixTitle')),'String');
            steps = get(gui.controls(gui.handleMap.('STpopup')),'String');
            xys   = get(gui.controls(gui.handleMap.('XYpopup')),'String');
            its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
            chns  = get(gui.controls(gui.handleMap.('CHpopup')),'String');
            nzval = get(gui.controls(gui.handleMap.('choosenstack')),'String');
            save([filepath,filename],...
                 'frameset','date','lastversion',...
                 'FSEGtable','FSEGfiltpop',...
                 'PSEGtable','PSEGfiltpop',...
                 'SPOTtable','SPOTfiltpop',...
                 'prefix','steps','xys','its','chns','nzval');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = HloadSetbutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uigetfile(['./','.mat'],'Load analysis');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            if(~isfield(loadin,'frameset'))
                display('File has no frameset');
                return;
            end
            frameset = loadin.frameset;
            if(~isfield(loadin,'PrefixTitle'))
                % reconstruct the channel names from the set
                [nsts,nxys,nchs,nits] = size(frameset);
                steps = {};
                xys   = {};
                chns  = {};
                its   = {};
                for s = 1:nsts
                    steps{s} = frameset{s,1,1,1}.step;
                end
                for xy = 1:nxys
                    xys{xy}  = frameset{1,xy,1,1}.xy;
                end
                for ch = 1:nchs
                    chns{ch} = frameset{1,1,ch,1}.chn;
                end
                for it = 1:nits
                    its{it}  = frameset{1,1,1,it}.it;
                end
                loadin.prefix = frameset{1,1,1,1}.prefix;
                loadin.steps  = steps;
                loadin.xys    = xys;
                loadin.its    = its;
                loadin.chns   = chns;
            end
            set(gui.controls(gui.handleMap.('PrefixTitle')),'String',loadin.prefix);
            set(gui.controls(gui.handleMap.('STpopup')),'String',loadin.steps);
            set(gui.controls(gui.handleMap.('XYpopup')),'String',loadin.xys);
            set(gui.controls(gui.handleMap.('ITpopup')),'String',loadin.its);
            set(gui.controls(gui.handleMap.('CHpopup')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('redpopup')),'String',{'none',loadin.chns{:}});
            set(gui.controls(gui.handleMap.('bluepopup')),'String',{'none',loadin.chns{:}});
            set(gui.controls(gui.handleMap.('greenpopup')),'String',{'none',loadin.chns{:}});
            set(gui.controls(gui.handleMap.('STpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('redpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('bluepopup')),'Value',1);
            set(gui.controls(gui.handleMap.('greenpopup')),'Value',1);
            if(isfield(loadin,'fileMode'))
                set(gui.controls(gui.handleMap.fileModepop),'Value',loadin.fileMode);
            end
            if(isfield(loadin,'projMode'))
                set(gui.controls(gui.handleMap.projModepop),'Value',loadin.projMode);
            end
            if(isfield(loadin,'FSEGtable'))
                set(gui.tables(gui.handleMap.FSEGtable),'Data',loadin.FSEGtable);
            end
            if(isfield(loadin,'PSEGtable'))
                set(gui.tables(gui.handleMap.PSEGtable),'Data',loadin.PSEGtable);
            end
            if(isfield(loadin,'SPOTtable'))
                set(gui.tables(gui.handleMap.SPOTtable),'Data',loadin.SPOTtable);
            end
            if(isfield(loadin,'BSEGtable'))
                set(gui.tables(gui.handleMap.BSEGtable),'Data',loadin.BSEGtable);
            end
            if(isfield(loadin,'FSEGfiltpop'))
                set(gui.controls(gui.handleMap.('FSEGfiltpop')),'Value',loadin.FSEGfiltpop);
            end
            if(isfield(loadin,'PSEGfiltpop'))
                set(gui.controls(gui.handleMap.('PSEGfiltpop')),'Value',loadin.PSEGfiltpop);
            end
            if(isfield(loadin,'SPOTfiltpop'))
                set(gui.controls(gui.handleMap.('SPOTfiltpop')),'Value',loadin.SPOTfiltpop);
            end
            if(isfield(loadin,'BSEGfiltpop'))
                set(gui.controls(gui.handleMap.('BSEGfiltpop')),'Value',loadin.BSEGfiltpop);
            end
            if(isfield(loadin,'nzval'))
                set(gui.controls(gui.handleMap.('choosenstack')),'String',loadin.nzval);
            end
            updateIMGpop_Callback();
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [frame,nst,nxy,nch,nit] = getcurrentframe()
        nst    = get(gui.controls(gui.handleMap.('STpopup')),'Value');
        nxy    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        nit    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        nch    = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        frame  = frameset{nst,nxy,nch,nit};
    end
    
    %% Image update
    function [] = updateIMGpop_Callback(~,~)
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [img] = loadimg(frame);
        axes(gui.axes(gui.handleMap.imageAxes));
        cla(gui.axes(gui.handleMap.imageAxes));
        gui.imgs = imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        updateCells();
        setMouseAction(gui.imgs);
    end 
    function [] = updateIMGproj_Callback(~,~)
        if(~isempty(frameset))
            updateIMGpop_Callback();
        end
    end
    function [img] = loadimg(frame)
        if(strcmp(frame.filepath(end-3:end),'.mat'))
            temp     = load(frame.filepath);
            [~,~,nz] = size(temp.img);
            img = stack2img(temp.img,nz);
        else
            [stack,nstacks] = loadTIFstack(frame.filepath);
            [img] = stack2img(stack,nstacks);
        end
        img = double(img);
        rot = str2num(get(gui.controls(gui.handleMap.rotateIMG),'String'));
        if(~rot==0)
            img = rotateimg(img,rot);
        end
    end
    function [img] = stack2img(stack,nstacks)
        if(nstacks==1)
            img = stack(:,:,1);
        else
            projmode = get(gui.controls(gui.handleMap.('projModepop')),'Value');
            if(projmode==1)
                img = maxproject(stack);
            elseif(projmode==2)
                img = meanproject(stack);
            elseif(projmode==3)
                img = sumproject(stack);
            elseif(projmode==4)
                ns = floor(str2num(get(gui.controls(gui.handleMap.choosenstack),'String')));
                [~,~,nz] =size(stack);
                if(ns>0 && ns<=nz)
                    img = stack(:,:,ns);
                else
                    img = zeros(size(stack(:,:,1)));
                end
            elseif(projmode==5)
                [img,z] = maxgradient(stack);
                set(gui.controls(gui.handleMap.choosenstack),'String',num2str(z));
            end
        end
    end
    function [] = updateCells()
        [frame,nst,nxy,nch,nit] = getcurrentframe();        
        if(isfield(frameset{nst,nxy,nch,nit},'cells'))
            cells    = frameset{nst,nxy,nch,nit}.cells;
            if(~isfield(frameset{nst,nxy,nch,nit},'selectedcells'))
                frameset{nst,nxy,nch,nit}.selectedcells = ones(size(cells));
            end
            selected = frameset{nst,nxy,nch,nit}.selectedcells;
            hold on;
            for i = 1:length(cells)
                border = cells{i}.border;
                if(selected(i)==0)
                    color = 'g';
                else
                    color = 'r';
                end
                h = plot(border(:,2),border(:,1),color,'LineWidth',1);
                h.UserData = i;
                set(h,'ButtonDownFcn',@selectCell);
            end
        end
    end
    %% Merge
    function [] = updateMergeIMG_Callback(~,~)
        [frame,nst,nxy,~,nit] = getcurrentframe();
        redchn   = get(gui.controls(gui.handleMap.('redpopup')),'Value')-1;
        bluechn  = get(gui.controls(gui.handleMap.('bluepopup')),'Value')-1;
        greenchn = get(gui.controls(gui.handleMap.('greenpopup')),'Value')-1;
        table    = get(gui.tables(gui.handleMap.MERGEtable),'Data');
        zimg = zeros(size(loadimg(frame)));
        rimg = zimg;
        bimg = zimg;
        gimg = zimg;
        if(redchn>0)
            rimg = loadimg(frameset{nst,nxy,redchn,nit});
            rimg = (rimg-table{1,2})./(table{2,2}-table{1,2});
        end
        if(bluechn>0)
            bimg = loadimg(frameset{nst,nxy,bluechn,nit});
            bimg = (bimg-table{1,4})./(table{2,4}-table{1,4});
        end
        if(greenchn>0)
            gimg = loadimg(frameset{nst,nxy,greenchn,nit});
            gimg = (gimg-table{1,6})./(table{2,6}-table{1,6});
        end
        tcolor = cat(3,rimg,gimg,bimg);
        axes(gui.axes(2));
        cla(gui.axes(2));
        image(tcolor);
        set(gui.axes(2),'xtick',[]);
        set(gui.axes(2),'ytick',[]);
    % $$$         [img] = loadimg(frame);
% $$$         cla(gui.axes(gui.handleMap.imageAxes));
% $$$         gui.imgs = imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
% $$$         updateCells();
% $$$         setMouseAction(gui.imgs);
    end 
    function GuessMergeTable_Callback(~,~)
        [frame,nst,nxy,~,nit] = getcurrentframe();
        redchn   = get(gui.controls(gui.handleMap.('redpopup')),'Value')-1;
        bluechn  = get(gui.controls(gui.handleMap.('bluepopup')),'Value')-1;
        greenchn = get(gui.controls(gui.handleMap.('greenpopup')),'Value')-1;
        table    = get(gui.tables(gui.handleMap.MERGEtable),'Data');
        if(redchn>0)
            rimg   = loadimg(frameset{nst,nxy,redchn,nit});            
            values = rimg(:);
            table{1,2} = min(values);
            table{2,2} = max(values);
        end
        if(bluechn>0)
            bimg   = loadimg(frameset{nst,nxy,bluechn,nit});            
            values = bimg(:);
            table{1,4} = min(values);
            table{2,4} = max(values);
        end
        if(greenchn>0)
            gimg   = loadimg(frameset{nst,nxy,greenchn,nit});            
            values = gimg(:);
            table{1,6} = min(values);
            table{2,6} = max(values);
        end
        set(gui.tables(gui.handleMap.MERGEtable),'Data',table);
    end

    %% Navigation for Microscope-Control
    function [] = moveUpZ(~,~)
        cz = floor(str2num(get(gui.controls(gui.handleMap.choosenstack),'String')));
        set(gui.controls(gui.handleMap.choosenstack),'String',num2str(cz+1));
        updateIMGpop_Callback();
    end
    function [] = moveDwZ(~,~)
        cz = floor(str2num(get(gui.controls(gui.handleMap.choosenstack),'String')));
        set(gui.controls(gui.handleMap.choosenstack),'String',num2str(cz-1));
        updateIMGpop_Callback();
    end
    function [] = moveUpIT(~,~)
        its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits   = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==length(its)))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==1))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==length(chns)))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==1))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys   = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==length(xys)))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==1))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys-1);
        end
        updateIMGpop_Callback();
    end
    
    %% Merge
    function [] = MergemoveUpIT(~,~)
        its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits   = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==length(its)))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits+1);
        end
        updateIMGpop_Callback();
        updateMergeIMG_Callback();
    end
    function [] = MergemoveDwIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==1))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits-1);
        end
        updateIMGpop_Callback();
        updateMergeIMG_Callback();
    end
    function [] = MergemoveUpXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys   = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==length(xys)))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys+1);
        end
        updateIMGpop_Callback();
        updateMergeIMG_Callback();
    end
    function [] = MergemoveDwXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==1))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys-1);
        end
        updateIMGpop_Callback();
        updateMergeIMG_Callback();
    end
    
    function LoadParams_FSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.FSEGtable;
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
            set(gui.controls(gui.handleMap.('FSEGfiltpop')),'Value',loadin.FSEGfiltpop);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveParams_FSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.FSEGtable;
        FSEGfiltpop = get(gui.controls(gui.handleMap.('FSEGfiltpop')),'Value');
        [filename,filepath] = uiputfile(['./','.mat'],'Save params table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            date  = getdate();
            save([filepath,filename],'table','date','FSEGfiltpop','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function LoadParams_PSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.PSEGtable;
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
            set(gui.controls(gui.handleMap.('PSEGfiltpop')),'Value',loadin.PSEGfiltpop);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveParams_PSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.PSEGtable;
        PSEGfiltpop = get(gui.controls(gui.handleMap.('PSEGfiltpop')),'Value');
        [filename,filepath] = uiputfile(['./','.mat'],'Save params table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            date  = getdate();
            save([filepath,filename],'table','date','PSEGfiltpop','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    function LoadParams_SPOT_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.SPOTtable;
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
            set(gui.controls(gui.handleMap.('SPOTfiltpop')),'Value',loadin.SPOTfiltpop);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveParams_SPOT_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.SPOTtable;
        SPOTfiltpop = get(gui.controls(gui.handleMap.('SPOTfiltpop')),'Value');
        [filename,filepath] = uiputfile(['./','.mat'],'Save params table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            date  = getdate();
            save([filepath,filename],'table','date','SPOTfiltpop','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function LoadParams_MSEG_Callback(Hobj,~)
        LoadParams_Aux(Hobj,gui.handleMap.MSEGtable,'');
    end
    function SaveParams_MSEG_Callback(Hobj,~)
        SaveParams_Aux(Hobj,gui.handleMap.MSEGtable,'');
    end
    
    function LoadParams_Aux(Hobj,handle,popID)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveParams_Aux(Hobj,handle,popID)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uiputfile(['./','.mat'],'Save params table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            date  = getdate();
            save([filepath,filename],'table','date','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function LoadParams_BSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.BSEGtable;
        [filename,filepath] = uigetfile(['./','.mat'],'Load table');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            set(gui.tables(handle),'Data',loadin.table);
            set(gui.controls(gui.handleMap.('BSEGfiltpop')),'Value',loadin.BSEGfiltpop);
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function SaveParams_BSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        handle = gui.handleMap.BSEGtable;
        BSEGfiltpop = get(gui.controls(gui.handleMap.('BSEGfiltpop')),'Value');
        [filename,filepath] = uiputfile(['./','.mat'],'Save params table');
        if(~(filename==0))
            table = get(gui.tables(handle),'Data');
            date  = getdate();
            save([filepath,filename],'table','date','BSEGfiltpop','lastversion');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    function [filts,angles,na] = getfiltvalues(filtpop)
        files  = get(gui.controls(gui.handleMap.(filtpop)),'String');
        nfile  = get(gui.controls(gui.handleMap.(filtpop)),'Value');
        temp   = load([gui.FSEGfiltfolder,files{nfile}]);
        filts  = temp.filts;
        angles = temp.angles;
        na     = temp.na;
    end
    
    %% FSEG functions
    function [] = FluoCut_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
        [frame] = getcurrentframe();
        mask0 = mytheimage(loadimg(frame),ptable{1,2});
        gui.imgs = imshow(mask0,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = FluoScore_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('FSEGfiltpop');
        img   = loadimg(frame);
        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getFSEGscore(img,filts,angles,na);
        gui.imgs = imshow(score,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end
    function [] = FluoMask_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('FSEGfiltpop');
        img   = loadimg(frame);
        mask0 = mytheimage(img,ptable{1,2});
        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getFSEGscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        gui.imgs = imshow(mask,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = FSEGrun1_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('FSEGfiltpop');
        img   = loadimg(frame);
        mask0 = mytheimage(img,ptable{1,2});
        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getFSEGscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        frameset{nst,nxy,nch,nit}.cells = mask2cells(mask);        
        frameset{nst,nxy,nch,nit}.segmethod = 'FSEG';
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frameset{nst,nxy,nch,nit}.cells));
        if(ptable{6,2}>0)
            [scells] = smoothframe(frameset{nst,nxy,nch,nit},...
                                   ptable{6,2},...
                                   zeros(size(frameset{nst,nxy,nch,nit}.selectedcells)));
            frameset{nst,nxy,nch,nit}.cells = scells;        
        end
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = FSEGrunAll_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        nst    = length(get(gui.controls(gui.handleMap.('STpopup')),'String'));
        nxy    = length(get(gui.controls(gui.handleMap.('XYpopup')),'String'));
        nit    = length(get(gui.controls(gui.handleMap.('ITpopup')),'String'));
        ch     = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
        [filts,angles,na] = getfiltvalues('FSEGfiltpop');
        for st = 1:nst
            for xy = 1:nxy
                for it = 1:nit
                    frame  = frameset{st,xy,ch,it};
                    set(gui.controls(gui.handleMap.imageValue),'String',frame.filename);
                    pause(0.05);
                    img   = loadimg(frame);
                    mask0 = mytheimage(img,ptable{1,2});
                    img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
                    [score] = getFSEGscore(img,filts,angles,na);
                    mask    = mask0.*mytheimage(score,ptable{5,2});
                    mask    = bwareaopen(mask,ptable{2,2});
                    mask    = double(imfill(mask,'holes'));
                    frameset{st,xy,ch,it}.cells = mask2cells(mask);
                    frameset{st,xy,ch,it}.segmethod = 'FSEG';
                    frameset{st,xy,ch,it}.selectedcells = ones(size(frameset{st,xy,ch,it}.cells));
                    if(ptable{6,2}>0)
                        [scells] = smoothframe(frameset{st,xy,ch,it},...
                                               ptable{6,2},...
                                               zeros(size(frameset{st,xy,ch,it}.selectedcells)));
                        frameset{st,xy,ch,it}.cells = scells;        
                    end
                end
            end
        end
        updateIMGpop_Callback();
        set(gui.controls(gui.handleMap.imageValue),'String','Segmentation done!');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% PSEG functions
    function [] = PhaseCut_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
        [frame] = getcurrentframe();
        [img] = loadimg(frame);
        mask0 = 1-mytheimage(img,ptable{1,2});
        gui.imgs = imshow(mask0,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = PhaseScore_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('PSEGfiltpop');
        img = loadimg(frame);
        img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getPSEGscore(img,filts,angles,na);
        gui.imgs = imshow(score,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end
    function [] = PhaseMask_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('PSEGfiltpop');
        img = loadimg(frame);
        mask0 = 1-mytheimage(img,ptable{1,2});
        img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getPSEGscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        gui.imgs = imshow(mask,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = PSEGrun1_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na] = getfiltvalues('PSEGfiltpop');
        img = loadimg(frame);
        mask0 = 1-mytheimage(img,ptable{1,2});
        img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getPSEGscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        frameset{nst,nxy,nch,nit}.cells = mask2cells(mask);        
        frameset{nst,nxy,nch,nit}.segmethod = 'PSEG';
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frameset{nst,nxy,nch,nit}.cells));
        if(ptable{6,2}>0)
            [scells] = smoothframe(frameset{nst,nxy,nch,nit},...
                                   ptable{6,2},...
                                   zeros(size(frameset{nst,nxy,nch,nit}.selectedcells)));
            frameset{nst,nxy,nch,nit}.cells = scells;        
        end
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = PSEGrunAll_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        nst    = length(get(gui.controls(gui.handleMap.('STpopup')),'String'));
        nxy    = length(get(gui.controls(gui.handleMap.('XYpopup')),'String'));
        nit    = length(get(gui.controls(gui.handleMap.('ITpopup')),'String'));
        ch     = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
        [filts,angles,na] = getfiltvalues('PSEGfiltpop');
        for st = 1:nst
            for xy = 1:nxy
                for it = 1:nit
                    frame  = frameset{st,xy,ch,it};
                    set(gui.controls(gui.handleMap.imageValue),'String',frame.filename);
                    pause(0.05);
                    img = loadimg(frame);
                    mask0 = 1-mytheimage(img,ptable{1,2});
                    img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
                    [score] = getPSEGscore(img,filts,angles,na);
                    mask    = mask0.*mytheimage(score,ptable{5,2});
                    mask    = bwareaopen(mask,ptable{2,2});
                    mask    = double(imfill(mask,'holes'));
                    frameset{st,xy,ch,it}.cells = mask2cells(mask);
                    frameset{st,xy,ch,it}.segmethod = 'PSEG';
                    frameset{st,xy,ch,it}.selectedcells = ones(size(frameset{st,xy,ch,it}.cells));
                    if(ptable{6,2}>0)
                        [scells] = smoothframe(frameset{st,xy,ch,it},...
                                               ptable{6,2},...
                                               zeros(size(frameset{st,xy,ch,it}.selectedcells)));
                        frameset{st,xy,ch,it}.cells = scells;        
                    end
                end
            end
        end
        set(gui.controls(gui.handleMap.imageValue),'String','Segmentation done!');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% SPOT functions
    function [] = SpotCut_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.SPOTtable),'Data');
        [frame] = getcurrentframe();
        [img] = loadimg(frame);
        mask0 = mytheimage(img,ptable{1,2});
        gui.imgs = imshow(mask0,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SpotScore_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.SPOTtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('SPOTfiltpop');
        img = loadimg(frame);
        img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getSPOTscore(img,filts,angles,na);
        gui.imgs = imshow(score,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end
    function [] = SpotMask_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        ptable = get(gui.tables(gui.handleMap.SPOTtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('SPOTfiltpop');
        img = loadimg(frame);
        mask0 = mytheimage(img,ptable{1,2});
        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getSPOTscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        gui.imgs = imshow(mask,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SPOTrun1_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        ptable = get(gui.tables(gui.handleMap.SPOTtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('SPOTfiltpop');
        img = loadimg(frame);
        mask0 = mytheimage(img,ptable{1,2});
        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getSPOTscore(img,filts,angles,na);
        mask    = mask0.*mytheimage(score,ptable{5,2});
        mask    = bwareaopen(mask,ptable{2,2});
        mask    = double(imfill(mask,'holes'));
        frameset{nst,nxy,nch,nit}.cells = mask2cells(mask);        
        frameset{nst,nxy,nch,nit}.spomethod = 'SPOT';
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frameset{nst,nxy,nch,nit}.cells));
        if(ptable{6,2}>0)
            [scells] = smoothframe(frameset{nst,nxy,nch,nit},...
                                   ptable{6,2},...
                                   zeros(size(frameset{nst,nxy,nch,nit}.selectedcells)));
            frameset{nst,nxy,nch,nit}.cells = scells;        
        end
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    %% Mother Machine segmentation
    function [] = MSEGrun1_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        ptable = get(gui.tables(gui.handleMap.MSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        img   = loadimg(frame);
        params = struct;
        for i = 1:size(ptable,1)
            params.(ptable{i,1}) = ptable{i,2};
        end
        params.lfilt = [-1,-1,-1,1.5,3,1.5,-1,-1,-1]';
        frameset{nst,nxy,nch,nit}.cells = mmfluosegimg(img,params);
        frameset{nst,nxy,nch,nit}.segmethod = 'MSEG';
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frameset{nst,nxy,nch,nit}.cells));
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = MSEGrunAll_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
% $$$         nst    = length(get(gui.controls(gui.handleMap.('STpopup')),'String'));
% $$$         nxy    = length(get(gui.controls(gui.handleMap.('XYpopup')),'String'));
% $$$         nit    = length(get(gui.controls(gui.handleMap.('ITpopup')),'String'));
% $$$         ch     = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
% $$$         ptable = get(gui.tables(gui.handleMap.MSEGtable),'Data');
% $$$         for st = 1:nst
% $$$             for xy = 1:nxy
% $$$                 for it = 1:nit
% $$$                     frame  = frameset{st,xy,ch,it};
% $$$                     set(gui.controls(gui.handleMap.imageValue),'String',frame.filename);
% $$$                     pause(0.05);
% $$$                     img   = loadimg(frame);
% $$$                     
% $$$                     %frameset{st,xy,ch,it}.cells = mask2cells(mask);
% $$$ 
% $$$                 end
% $$$             end
% $$$         end
        set(gui.controls(gui.handleMap.imageValue),'String','Segmentation done!');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% BSEG
    function [] = BSEGMask_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('BSEGfiltpop');
        [img]   = loadimg(frame);
        img     = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getBSEGscore(img,filts,angles,na,ptable{7,2});
        mask0   = 1-mytheimage(img,ptable{1,2});
        mask1   = mytheimage(score,ptable{5,2}); % score threshold
        mask    = mask1-bwareaopen(mask1,ptable{6,2}); 
        mask    = double(imfill(bwareaopen(mask,ptable{2,2}),'holes'));
        %mask    = mask0.*mytheimage(score,ptable{5,2});
        %mask    = bwareaopen(mask,ptable{2,2});
        %mask    = double(imfill(mask,'holes'));
        gui.imgs = imshow(mask,[],'Parent',gui.axes(gui.handleMap.imageAxes));
    end
    function [] = BSEGScore_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('BSEGfiltpop');
        [img]   = loadimg(frame);
        img     = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getBSEGscore(img,filts,angles,na,ptable{7,2});
        gui.imgs = imshow(score,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end
    function [score] = getBSEGscore(img,filts,angles,na,inblack)
        [nx,ny] = size(img);
        fimgs = zeros(nx,ny,na);
        score = zeros(nx,ny);
        if(inblack==1)
            for i = 1:na
                fimgs(:,:,i) = imfilter(img,filts{i},'replicate','conv');
            end
        else
            for i = 1:na
                fimgs(:,:,i) = imfilter(img,-1*filts{i},'replicate','conv');
            end
        end
        fimgs2 = fimgs(:,:,1:na/2)+fimgs(:,:,na/2+1:end);
        score(:) = sum(reshape(fimgs2.*real(acos(-1*pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
        score    = score.*img;
        score    = -1*score./(max(max(score)));
    end
    
    function [] = BSEGCut_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
        [frame] = getcurrentframe();
        [img] = loadimg(frame);
        mask0 = 1-mytheimage(img,ptable{1,2});
        gui.imgs = imshow(mask0,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end
    function [] = BSEGrun1_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [filts,angles,na]       = getfiltvalues('BSEGfiltpop');
        [img]   = loadimg(frame);
        img     = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
        [score] = getBSEGscore(img,filts,angles,na,ptable{7,2});
        mask0   = 1-mytheimage(img,ptable{1,2});
        mask1   = mytheimage(score,ptable{5,2}); % score threshold
        mask    = mask1-bwareaopen(mask1,ptable{6,2}); 
        mask    = double(imfill(bwareaopen(mask,ptable{2,2}),'holes'));
        frameset{nst,nxy,nch,nit}.cells = mask2cells(mask);        
        frameset{nst,nxy,nch,nit}.segmethod = 'BSEG';
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frameset{nst,nxy,nch,nit}.cells));
        updateIMGpop_Callback();
    end
    function [] = BSEGrunAll_Callback(Hobj,~) 
        ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
        [filts,angles,na]       = getfiltvalues('BSEGfiltpop');
        nst    = length(get(gui.controls(gui.handleMap.('STpopup')),'String'));
        nxy    = length(get(gui.controls(gui.handleMap.('XYpopup')),'String'));
        nit    = length(get(gui.controls(gui.handleMap.('ITpopup')),'String'));
        ch     = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        for st = 1:nst
            for xy = 1:nxy
                for it = 1:nit
                    frame  = frameset{st,xy,ch,it};
                    set(gui.controls(gui.handleMap.imageValue),'String',frame.filename);
                    pause(0.05);
                    img = loadimg(frame);
                    img     = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
                    [score] = getBSEGscore(img,filts,angles,na,ptable{7,2});
                    mask0   = 1-mytheimage(img,ptable{1,2});
                    mask1   = mytheimage(score,ptable{5,2}); % score threshold
                    mask    = mask1-bwareaopen(mask1,ptable{6,2}); 
                    mask    = double(imfill(bwareaopen(mask,ptable{2,2}),'holes'));
                    frameset{st,xy,ch,it}.cells = mask2cells(mask);        
                    frameset{st,xy,ch,it}.segmethod = 'BSEG';
                    frameset{st,xy,ch,it}.selectedcells = ones(size(frameset{st,xy,ch,it}.cells));
                end
            end
        end
        updateIMGpop_Callback();
        set(gui.controls(gui.handleMap.imageValue),'String','Segmentation done!');
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    %% Refinement
    function [] = BlinkCells_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        scells = frame.selectedcells;
        newcells = {};
        ccount = 0;
        for ni = 1:length(frame.cells)
            if(scells(ni)==1)
                ccount = ccount+1;
                newcells{ccount} = frame.cells{ni};
            end
        end
        frameset{nst,nxy,nch,nit}.cells = newcells;
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
        updateIMGpop_Callback(); pause(0.5);
        frameset{nst,nxy,nch,nit}.cells = frame.cells;
        frameset{nst,nxy,nch,nit}.selectedcells = frame.selectedcells;
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SelectedNoneCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(frame.selectedcells));
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SelectedAllCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        frameset{nst,nxy,nch,nit}.selectedcells = zeros(size(frame.selectedcells));
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function [] = RemoveBorderCell_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        udata  = get(Hobj,'UserData');
        rtable = get(gui.tables(udata),'Data');
        delta  = rtable{6,2};
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [nr,nc]  = size(loadimg(frame));
        rowpos = repmat([1:nr]',1,nc);
        colpos = repmat([1:nc],nc,1);
        % find cells to remove
        if(~isfield(frame,'selectedcells'))
            return;
        end
        scells = ones(size(frame.selectedcells));
        for ni = 1:length(frame.cells)
            pixs = frame.cells{ni}.PixelIdxList;
            extrs = [min(rowpos(pixs)),max(rowpos(pixs)),...
                     min(colpos(pixs)),max(colpos(pixs))];
            ds = abs(extrs-[1,nr,1,nc]);
            %f  = find(ds<=delta);
            if(~isempty(find(ds<=delta)))
                scells(ni) = 0;
            end
        end
        frameset{nst,nxy,nch,nit}.selectedcells = scells;
        % show them
        updateIMGpop_Callback();
        pause(0.05);
        % remove them
        newcells = {};
        ccount = 0;
        for ni = 1:length(frame.cells)
            if(scells(ni)==1)
                ccount = ccount+1;
                newcells{ccount} = frame.cells{ni};
            end
        end
        frameset{nst,nxy,nch,nit}.cells = newcells;
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = RemoveSelectedCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        scells = frame.selectedcells;
        newcells = {};
        ccount = 0;
        for ni = 1:length(frame.cells)
            if(scells(ni)==1)
                ccount = ccount+1;
                newcells{ccount} = frame.cells{ni};
            end
        end
        frameset{nst,nxy,nch,nit}.cells = newcells;
        frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = MergeSelectedCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        udata  = get(Hobj,'UserData');
        rtable = get(gui.tables(udata),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        scells = frame.selectedcells;
        img    = loadimg(frame);
        se     = strel('disk',rtable{4,2});
        newcells = {};
        ccount = 0;
        mask = zeros(size(img));
        initncells = length(frame.cells);
        for ni = 1:length(frame.cells)
            if(scells(ni)==1) % not selected
                ccount = ccount+1;
                newcells{ccount} = frame.cells{ni};
            else
                mask(frame.cells{ni}.PixelIdxList)=1;    
            end
        end
        mask  = imdilate(mask,se);
        mask  = imerode(mask,se);
        mcells = mask2cells(mask);
        for ni = 1:length(mcells)
            ccount = ccount+1;
            newcells{ccount} = mcells{ni};
        end
        frameset{nst,nxy,nch,nit}.cells = newcells;
        endncells = length(newcells);
        if(~(endncells==initncells))
            frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
        end
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = MergeSelectedCellsMSEG_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        %udata  = get(Hobj,'UserData');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        scells = frame.selectedcells;
        cells  = frame.cells;
        mcellspos = find(scells==0);
        if(length(mcellspos==2))
            mcell = mmmergecells(cells{mcellspos(1)},cells{mcellspos(2)});
            if(mcellspos(1)==(mcellspos(2)-1))
                newcells = {cells{1:(mcellspos(1)-1)},...
                            mcell,...
                            cells{mcellspos(2)+1:end}};
                frameset{nst,nxy,nch,nit}.cells = newcells;
                frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
                updateIMGpop_Callback();
            else
                display('Merging not implemented');
            end
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function [] = SplitSelectedCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor); pause(0.05);
        udata  = get(Hobj,'UserData');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        if(udata==2) % FSEG
            ptable = get(gui.tables(gui.handleMap.FSEGtable),'Data');
            rtable = get(gui.tables(gui.handleMap.FSEGreftable),'Data');
            [filts,angles,na] = getfiltvalues('FSEGfiltpop');
            img   = loadimg(frame);
            mask0 = mytheimage(img,ptable{1,2});
            img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
            [score] = getFSEGscore(img,filts,angles,na);
            img2    = eval(rtable{3,2});
        elseif(udata==4) % PSEG
            ptable = get(gui.tables(gui.handleMap.PSEGtable),'Data');
            rtable = get(gui.tables(gui.handleMap.PSEGreftable),'Data');
            [filts,angles,na] = getfiltvalues('PSEGfiltpop');
            img = loadimg(frame);
            mask0 = 1-mytheimage(img,ptable{1,2});
            img = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
            [score] = getPSEGscore(img,filts,angles,na);
            img2    = eval(rtable{3,2});
        elseif(udata==10) % BSEG
            ptable = get(gui.tables(gui.handleMap.BSEGtable),'Data');
            rtable = get(gui.tables(gui.handleMap.BSEGreftable),'Data');
            [frame,nst,nxy,nch,nit] = getcurrentframe();
            [filts,angles,na]       = getfiltvalues('BSEGfiltpop');
            [img]   = loadimg(frame);
            img     = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
            [score] = getBSEGscore(img,filts,angles,na,ptable{7,2});
            if(strcmp(rtable{3,2},'img')&&(ptable{7,2}==1))
                img2    = imcomplement(img);
            else
                img2    = eval(rtable{3,2});
            end
        end
% $$$         mask    = mask0.*mytheimage(score,ptable{5,2});
% $$$         mask    = bwareaopen(mask,ptable{2,2});
% $$$         mask    = double(imfill(mask,'holes'));
        scells  = frame.selectedcells; %score.*img
        
        newcells = {};
        ccount = 0;
        initncells = length(frame.cells);
        for ni = 1:length(frame.cells)
            if(scells(ni)==1) % not selected
                ccount = ccount+1;
                newcells{ccount} = frame.cells{ni};
            else(scells(ni)==0) % not selected
                cvals = img2(frame.cells{ni}.PixelIdxList);
                [~,sind] = sort(cvals,'ascend');
                rmvp = max([1,floor(rtable{2,2}*length(cvals))]);
                mask2 = zeros(size(img));
                mask2(frame.cells{ni}.PixelIdxList(sind(rmvp:end)))=1;
                splitcells = mask2cells(mask2);
                display(['Cell ',num2str(ni),' split in ',num2str(length(splitcells))]);
                for nc = 1:length(splitcells)
                    ccount = ccount+1;
                    newcells{ccount} = splitcells{nc};
                end
            end
        end
        frameset{nst,nxy,nch,nit}.cells = newcells;
        endncells = length(newcells);
        if(~(endncells==initncells))
            frameset{nst,nxy,nch,nit}.selectedcells = ones(size(newcells));
        end
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SmoothAllCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        udata  = get(Hobj,'UserData');
        rtable = get(gui.tables(udata),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [scells] = smoothframe(frame,rtable{1,2},zeros(size(frame.selectedcells)));
        frameset{nst,nxy,nch,nit}.cells = scells;
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SmoothSelectedCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        udata  = get(Hobj,'UserData');
        rtable = get(gui.tables(udata),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [scells] = smoothframe(frame,rtable{1,2},frame.selectedcells);
        frameset{nst,nxy,nch,nit}.cells = scells;
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = ReshapeSelectedCells_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);pause(0.05);
        udata  = get(Hobj,'UserData');
        rtable = get(gui.tables(udata),'Data');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        skipneg = frame.selectedcells;
        img   = loadimg(frame);
        se    = strel('disk',abs(rtable{5,2}));
        for ni = 1:length(frame.cells)
            if(skipneg(ni)==0)
                mask = zeros(size(img));
                mask(frame.cells{ni}.PixelIdxList)=1;
                if(rtable{4,2}>0)
                    mask  = imdilate(mask,se);
                else
                    mask  = imerode(mask,se);
                end
                cells = mask2cells(mask);
                frame.cells{ni} = cells{1};
            end
        end
        frameset{nst,nxy,nch,nit}.cells = frame.cells;
        updateIMGpop_Callback();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [scells] = smoothframe(frame,diskr,skipneg) 
        img   = loadimg(frame);
        se    = strel('disk',diskr);
        for ni = 1:length(frame.cells)
            if(skipneg(ni)==0)
                frame.cells{ni} = smoothcell(img,frame.cells{ni},se);
            end
        end
        scells = frame.cells;
    end
    function [ocell] = smoothcell(img,cell,se)
        mask = zeros(size(img));
        mask(cell.PixelIdxList)=1;
        mask  = imdilate(mask,se);
        mask  = imerode(mask,se);
        cells = mask2cells(mask);
        ocell = cells{1};
    end

    %% Mouse capture
    function setMouseAction(imgh)
        nopt = get(gui.controls(gui.handleMap.FSEGMouseOptions),'Value');
        if(nopt==0)
            set(imgh,'ButtonDownFcn',@MouseShowValue);
        else
            % not in use for now
        end
    end
    function [] = MouseShowValue(Hobj,event)
        [x,y] = getPointerPos(event);
        img = get(Hobj,'Cdata');
        value = img(x,y);
        set(Hobj,'Cdata',img);
        str = ['x:',num2str(x),' ','y:',num2str(y),' ','int:',num2str(value)];
        set(gui.controls(gui.handleMap.imageValue),'String',str);
    end
    function [x,y] = getPointerPos(HitEvent)
        pos = HitEvent.IntersectionPoint;
        y = min([512,max([1,floor((512/510)*(pos(1)))])]);
        x = min([512,max([1,floor((512/510)*(pos(2)))])]);
    end
    function selectCell(Hobj,~)
        color   = get(Hobj,'Color');
        cellind = get(Hobj,'UserData');
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        if(color(1)==1)
            set(Hobj,'Color',[0,1,0]);
            frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 0;
        elseif(color(2)==1)
            set(Hobj,'Color',[1,0,0]);
            frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 1;
        end
    end

end