function [data] = gettcelldata(cells,nits,datastr,chn)

pix2um = 0.16;

ncells = length(cells);
data = zeros(ncells,nits);
for c = 1:ncells
    switch datastr
      case 'times'
        data(c,cells{c}.inframes) = cells{c}.times;
      case 'length'
        data(c,cells{c}.inframes) = pix2um*cells{c}.lengths;
      case 'growth rate'
        if(isfield(cells{c},'grates'))
            data(c,cells{c}.inframes) = cells{c}.grates;
        else
            data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
        end
      case 'area'
        data(c,cells{c}.inframes) = pix2um.^2*cells{c}.areas;
      case 'width'
        data(c,cells{c}.inframes) = pix2um.*cells{c}.widths;
      case 'intensity'
        data(c,cells{c}.inframes) = cells{c}.fluos(:,chn);
      case 'length birth'
        if(~cells{c}.parent==0)
            data(c,cells{c}.inframes) = pix2um.*cells{c}.lengths(1)*ones(size(cells{c}.inframes));
        else
            data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
        end
      case 'length div'
        if(~cells{c}.divisions==0)
            data(c,cells{c}.inframes) = pix2um.*cells{c}.lengths(end)*ones(size(cells{c}.inframes));
        else
            data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
        end
      case 'length delta'
        if(~cells{c}.divisions==0 && ~cells{c}.parent==0)
            data(c,cells{c}.inframes) = pix2um.*(cells{c}.lengths(end)-cells{c}.lengths(1))*ones(size(cells{c}.inframes));
        else
            data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
        end
      case 'generation time'
        if(~cells{c}.divisions==0 && ~cells{c}.parent==0)
            data(c,cells{c}.inframes) = (cells{c}.times(end)-cells{c}.times(1))*ones(size(cells{c}.inframes));
        else
            data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
        end
    end
end



end