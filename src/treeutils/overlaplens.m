function [fint] = overlaplens(c1,l1,c2,l2)
% how cells with centers and length overlap (in one axis)
% percentage relative to cell 2
    p11 = c1-l1/2;
    p12 = c1+l1/2;
    p21 = c2-l2/2;
    p22 = c2+l2/2;
    
    pmat = [p11,p12;p21,p22];
    [~,mpos1] = max(pmat(:,1));
    [~,mpos2] = min(pmat(:,2));
    
    dint = pmat(mpos2,2)-pmat(mpos1,1);
    if(dint<0)
        dint=0;
    end
    fint = dint./l2;

end