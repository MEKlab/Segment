function [] = makeMMCSEGmovie(name,nchb,segfiles,segchn,chn,params,maximg,minimg,fps,color)
    
    framesets = {};
    for f = 1:length(segfiles)
        segfile = segfiles{f};
        temp = load(segfile);
        framesets{f} = temp.frameset;
    end
    
    img = loadmatimg(framesets{1}{1,1,3,1});
    [nx,ny] = size(img);
    xmat = zeros(nx,ny);
    ymat = zeros(nx,ny);
    for x = 1:nx
        for y = 1:ny
            xmat(x,y) = x;
            ymat(x,y) = y;
        end
    end
    
    % identify the connected components from the each
    yframepos = {};
    totalccs = 0;
    for f = 1:length(segfiles)
        frameset = framesets{f};
        cells = frameset{1,1,segchn,1}.cells;
        [clusters,mpos] = clusterbypos(cells,10,'ascend');
        yframepos{f} = floor(mpos);
        totalccs = totalccs+length(mpos);
    end
    display(['Total number of chambers ',num2str(totalccs)]);
    
    x0 = 300;
    x1 = 490;
    dy = 21;
    dx = length(x0:x1);
    if(totalccs<=nchb)
        ndx = 1;
    elseif(totalccs>nchb && totalccs<=2*nchb)
        ndx = 2;
    elseif(totalccs>2*nchb && totalccs<=3*nchb)
        ndx = 3;
    elseif(totalccs>3*nchb && totalccs<=4*nchb)
        ndx = 4;
    else
        error('Too many. Not implemented');
    end

    [~,~,~,its] = size(framesets{1});

    fig     = figure('MenuBar','none','Position',[0,0,dy*nchb+20,dx*ndx+40]);
    ha      = axes('Units','Pixels','Position',[10,3,dy*nchb,dx*ndx]);
    vidObj = VideoWriter([name,'.avi']);
    set(vidObj,'FrameRate',fps);
    open(vidObj);
    for it = 1:its
        currcc = 0;
        plotimg = zeros(ndx*dx,dy*nchb);
        for f = 1:length(segfiles)
            [img,dtime] = loadmatimg(framesets{f}{1,1,chn,it});
            img  = rotateimg(img,params.rot);
            ycenters    = yframepos{f};
            for cc = 1:length(ycenters)
                y0 = ycenters(cc)-(dy-1)/2;
                y1 = ycenters(cc)+(dy-1)/2;
                if(y0<1)
                    y0 = 1;
                    y1 = dy;
                end
                if(y1>ny)
                    y0 = ny-dy+1;
                    y1 = ny;
                end
                simg = img(x0:x1,y0:y1);
                currcc = currcc+1;
                if(currcc<=nchb)
                    plotimg(1:dx,(dy*(currcc-1)+1):(dy*(currcc))) = simg;
                elseif(currcc<=2*nchb)
                    plotimg(dx+1:2*dx,(dy*(currcc-(nchb+1))+1):(dy*(currcc-nchb))) = simg;
                elseif(currcc<=3*nchb)
                    plotimg((2*dx+1):(3*dx),(dy*(currcc-(2*nchb+1))+1):(dy*(currcc-2*nchb))) = simg;
                elseif(currcc<=4*nchb)
                    plotimg((3*dx+1):end,(dy*(currcc-(3*nchb+1))+1):(dy*(currcc-3*nchb))) = simg;
                end
            end
        end
        plotimg = (plotimg-minimg)./maximg;
        tcolor = cat(3,plotimg,plotimg,plotimg);
        image(tcolor);
        set(ha,'xtick',[]);
        set(ha,'ytick',[]);
        hold on;
        currcc = 0;
        for f = 1:length(segfiles)
            cells    = framesets{f}{1,1,segchn,it}.cells;
            ycenters = yframepos{f};
            [clusters,mpos] = clusterbypos(cells,10,'ascend');
            for cc = 1:length(ycenters)
                y0 = ycenters(cc)-(dy-1)/2;
                y1 = ycenters(cc)+(dy-1)/2;
                if(y0<1)
                    y0 = 1;
                    y1 = dy;
                end
                if(y1>ny)
                    y0 = ny-dy+1;
                    y1 = ny;
                end
                currcc = currcc+1;
                if(currcc<=nchb)
                    cy = dy*(currcc-1);
                    cx = 0;
                    %plotimg(1:dx,(dy*(currcc-1)+1):(dy*(currcc))) = simg;
                elseif(currcc<=2*nchb)
                    cy = dy*(currcc-(nchb+1));
                    cx = dx;
                elseif(currcc<=3*nchb)
                    cy = dy*(currcc-(2*nchb+1));
                    cx = 2*dx;
                elseif(currcc<=4*nchb)
                    cy = dy*(currcc-(3*nchb+1));
                    cx = 3*dx;
                end
                for c = 1:length(cells)
                    ypos = cells{c}.center(1);
                    if(abs(ycenters(cc)-ypos)<10) 
                        % Draw here
                        border = cells{c}.border;
                        border(:,2) = border(:,2) - (y0-1) + cy;
                        border(:,1) = border(:,1) - (x0-1) + cx;
                        plot(border(:,2),border(:,1),color);
                    end
                end
            end
        end
        title([num2str(floor(dtime/60)),' min']);
        writeVideo(vidObj,getframe(gcf));
        cla();
    end
    close(vidObj);
    close(fig);
    
end