function [img,z] = maxgradient(stack)

    [nx,ny,ns] = size(stack);
    mgrads = zeros(1,ns);
    for s = 1:ns
        %mgrads(s) = mean2(abs(imgradient(stack(:,:,s))));
        mgrads(s) = mean2(abs(imgradient(stack(:,:,s)))./stack(:,:,s));
    end
    [~,z] = max(mgrads);
    img = stack(:,:,z);

end