function [stack,nstacks] = loadTIFstack(filepath)

info = imfinfo(filepath);
nstacks  = numel(info);

[nx,ny] = size(imread(filepath));
stack = zeros(nx,ny,nstacks);

for i = 1:nstacks
    stack(:,:,i) = double(imread(filepath,i, 'Info',info));
end

end