function [mproj] = sumproject(stack)

[nx,ny,ns] = size(stack);
mproj = zeros(nx,ny);
mproj(:) = sum(reshape(stack(:,:,:),nx*ny,ns)');

end