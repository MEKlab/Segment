function [mask] = mytheimage(img,th)

mask = zeros(size(img));
[nx,ny] = size(img);
for i = 1:nx
    for j = 1:ny
        if(img(i,j)>th)
            mask(i,j)=1;
        end
    end
end

end