function [] = treescatterplot(cells,maxit,datastr1,chn1,datastr2,chn2)

[~,~,ends,~,~,~] = ordertree(cells);
nits = max(ends);

data1  = gettcelldata(cells,nits,datastr1,chn1);
data2  = gettcelldata(cells,nits,datastr2,chn2);
vals   = [[],[]];
count  = 0;
switch datastr1
  case 'intensity'
    for c = 1:size(data1,1)
        invals1 = find(data1(c,:)>0);
        invals2 = find(data2(c,:)>0);
        if(length(invals1)>0 && length(invals2)>0)
            if(invals1(1)<maxit)
                count = count+1;
                vals = [vals;[data1(c,invals1)',data2(c,invals1)']];
            end
        end
    end
  otherwise
    for c = 1:size(data1,1)
        invals1 = find(data1(c,:)>0);
        invals2 = find(data2(c,:)>0);
        if(length(invals1)>0 && length(invals2)>0)
            if(invals1(1)<maxit)
                count = count+1;
                if(strcmp(datastr2,'growth rate'))
                    vals = [vals;[data1(c,invals1(1)),mean(data2(c,invals2))]];
                else
                    vals = [vals;[data1(c,invals1(1)),data2(c,invals1(1))]];
                end
            end
        end
    end
end
dscatter(vals(:,1),vals(:,2));

end