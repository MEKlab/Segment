function [] = treepopplot(cells,datastr,chn,p1,p2)

[~,~,ends,~,~,~] = ordertree(cells);
nits = max(ends);

times = gettcelldata(cells,nits,'times',chn);
data  = gettcelldata(cells,nits,datastr,chn);
hold on;
mvals  = zeros(1,nits);
mtimes = zeros(1,nits);
p1s   = zeros(1,nits);
p2s   = zeros(1,nits);
for it = 1:nits
    inframe = find(data(:,it)>0);
    vals    = data(inframe,it)';
    mvals(it)  = mean(vals);
    mtimes(it) = mean(times(inframe,it)');
    p1s(it)   = prctile(vals,p1);
    p2s(it)   = prctile(vals,p2);
    plot(times(inframe,it)',vals,'.b');
end
ppos = find(isnan(p2s)==0);
plot(mtimes,mvals,'r');
myupdownfill(mtimes(ppos),p2s(ppos),p1s(ppos),'r',0.5,0);

end