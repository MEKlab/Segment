function [] = treeplot(cells)

ncells = length(cells);
[index,~,~,~,starts,ends] = ordertree(cells);

colors = zeros(ncells,3);
for u = 1:ncells
    colors(u,:) = randcolor();
end

hold on;
for c = 1:ncells
    id = cells{c}.ID;
    if(~isempty(cells{c}.childs))
        childs = cells{c}.childs;
        if((childs(1)>0)&& (childs(2)>0))
            plot([index(childs(1));index(childs(2))],[starts(childs(1));starts(childs(2))],...
                 '-','Color','black','LineWidth',2);
            plot([index(c);index(c)],[starts(c);starts(childs(1))],'-','Color',colors(id,:),'LineWidth',2);
        else
            plot([index(c);index(c)],[starts(c);ends(c)],'-','Color',colors(id,:),'LineWidth',2);
        end
    end
    plot(index(c),starts(c),'.','Color',colors(id,:),'MarkerSize',5);
end
axis([-10 ncells+10 0 (max(ends)+10)]);

end