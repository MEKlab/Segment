addpath('../utils/');
addpath('../segutils/');

nastrs = {'9'};
dmstrs = {'2.0','2.5','3.0','3.5','4.0','5.0','6.0','7.0'};
sgstrs = {'0.50','0.75','1.00','2.00'};

for a = 1:length(nastrs)
    for d = 1:length(dmstrs)
        for s = 1:length(sgstrs)
            [filts,na,angles] = angledFilt(str2num(nastrs{a}),...
                                           str2num(dmstrs{d}),...
                                           str2num(sgstrs{s}));
            save(['NA-',nastrs{a},'_',...
                  'DM-',dmstrs{d},'_',...
                  'SG-',sgstrs{s},'.mat'],...
                 'filts','angles','na');
        end
    end
end
