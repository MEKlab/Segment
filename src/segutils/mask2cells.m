function [cells] = mask2cells(mask)

statprops = {'PixelIdxList','Centroid','MajorAxisLength','MinorAxisLength','Orientation','Area'};

B      = bwboundaries(mask);
cc     = bwconncomp(mask);
stats  = regionprops(cc,statprops{:});

cells = cell(length(stats),1);

for i = 1:length(stats)
    center           = [stats(i).Centroid(1),stats(i).Centroid(2)];
    cells{i}         = struct;
    cells{i}.center  = center;
    cells{i}.border  = B{i};
    cells{i}.PixelIdxList = stats(i).PixelIdxList;
    cells{i}.length = stats(i).MajorAxisLength;
    cells{i}.width  = stats(i).MinorAxisLength;
    cells{i}.angle  = stats(i).Orientation;
    cells{i}.area   = stats(i).Area;
end

end