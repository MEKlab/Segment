function [area] = border2area(cell)

xpos  = cell.midline(:,2);
edges = cell.edges-cell.midline(:,1);

% let's do a trapezoid
area = 2*trapz(xpos,edges(:,2));

end 