function [] = segFLUObatch(foldername,segchns,paramsfile,filtfile)
    lastversion = '20170920';
    
    currfolder = [fileparts(which('segFLUObatch')),'/'];
    addpath(genpath([currfolder,'/src/']));
    
    %% Load saved parameters
    temp1 = load(paramsfile);
    ptable = temp1.table;
    FSEGfiltpop = temp1.FSEGfiltpop;
    
    %% Load filter values
    temp2 = load(filtfile);
    filts = temp2.filts;
    angles = temp2.angles;
    na = temp2.na;
    
    frameset = {};
    if(strcmp(foldername(end-3:end),'.mat'))
        display([' Loading set ',foldername]);
        temp = load(foldername);
        frameset = temp.('frameset');
        prefix = temp.('prefix');
        steps = temp.('steps');
        xys = temp.('xys');
        its = temp.('its');
        chns = temp.('chns');
    else
        display([' Analyzing folder ',foldername]);
        imgfiles = dir([foldername,'IMG-*.mat']);
        if(length(imgfiles)>0)
            
            %% Load all files
            [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);
            frameset = cell(length(steps),length(xys),length(chns),length(its));
            for s = 1:length(steps)
                for xy = 1:length(xys)
                    for ch = 1:length(chns)
                        for it = 1:length(its)
                            if(~strcmp(steps{s},''))
                                filename = ['IMG-',prefix,...
                                            '_ST-',steps{s},...
                                            '_XY-',xys{xy},...
                                            '_CHN-',chns{ch},...
                                            '_IT-',its{it},...
                                            '.mat'];
                            else
                                filename = ['IMG-',prefix,...
                                            '_XY-',xys{xy},...
                                            '_CHN-',chns{ch},...
                                            '_IT-',its{it},...
                                            '.mat'];
                            end
                            filepath = [foldername,filename];
                            if(~exist(filepath))
                                error(['File does not exists: ',filepath]);
                            end
                            frame = struct;
                            frame.prefix    = prefix;
                            frame.step      = steps{s};
                            frame.xy        = xys{xy};
                            frame.chn       = chns{ch};
                            frame.it        = its{it};
                            frame.folder    = foldername;
                            frame.filename  = filename;
                            frame.filepath  = filepath;
                            frameset{s,xy,ch,it} = frame;
                        end
                    end
                end            
            end
        end
    end
    %% Segment
    if(length(frameset)==0)
        display([' Failed loading set']);
    else
        nst    = length(steps);
        nxy    = length(xys);
        nit    = length(its);
        for st = 1:nst
            for xy = 1:nxy
                for it = 1:nit
                    for ch = segchns
                        frame  = frameset{st,xy,ch,it};
                        display(['  Segmenting IMG ',frame.filename]);
                        temp  = load(frame.filepath);
                        img   = double(temp.img);
                        mask0 = mytheimage(img,ptable{1,2});
                        img   = imfilter(img,fspecial('gaussian',ptable{3,2},ptable{4,2}),'replicate');
                        [nx,ny] = size(img);
                        fimgs = zeros(nx,ny,na);
                        for i = 1:na
                            fimgs(:,:,i) = imfilter(img,filts{i},'replicate','conv');
                        end
                        fimgs2 = fimgs(:,:,1:na/2)+fimgs(:,:,na/2+1:end);
                        score    = zeros(nx,ny);
                        score(:) = sum(reshape(fimgs2.*real(acos(pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
                        norms    = zeros(nx,ny);
                        norms(:) = sum(reshape(fimgs2.*real(acos(-1*pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
                        score    = score./(norms+1);
                        score    = exp(imcomplement(log(-1*score+1)))./log(10);
                        mask    = mask0.*mytheimage(score,ptable{5,2});
                        mask    = bwareaopen(mask,ptable{2,2});
                        mask    = double(imfill(mask,'holes'));
                        frameset{st,xy,ch,it}.cells = mask2cells(mask);
                        frameset{st,xy,ch,it}.selectedcells = ones(size(frameset{st,xy,ch,it}.cells));
                        if(ptable{6,2}>0)
                            
                        end
                    end
                end
            end
        end
    end
    outfile = ['./',prefix,'_segmented.mat'];
    date    = getdate();
    FSEGtable = ptable;
    save(outfile,...
         'frameset','date','lastversion',...
         'FSEGtable','filts','angles','na',...
         'FSEGfiltpop','prefix','steps','xys','its','chns');
    display([' Result saved in ',outfile]);

    end
end