function [frameset] = subsetGUI()

    lastversion = '20170830';
    
    currfolder = [fileparts(which('segmentGUI')),'/'];
    addpath(genpath([currfolder,'/src/']));
    
    gui = struct;
    gui.myblack = 0.2*[1,1,1];
    gui.mywhite = 0.99*[1,1,1];
    gui.defbkgcol = [0.94,0.94,0.94];
    gui.busycolor = 'yellow';
    gui.OFFcolor  = 'red';
    gui.ONcolor   = 'green';
    gui.FSEGfiltfolder = [currfolder,'/src/filters/'];
    
    gui.handleMap = struct;
    gui.folder    = '';
    gui.date = getdate();
    
    gui.figX = 1300;
    gui.figY = 720;
    
    Xoffset  = 0.005*gui.figX;
    Yoffset  = 0.005*gui.figY;
    dw1      = 5;
    bheight1 = 40;
    bwidth1   = 91;
    
    frameset = {};

    lastc = 0;
    % https://uk.mathworks.com/matlabcentral/answers/96816-how-do-i-change-the-default-background-color-of-all-figure-objects-created-in-matlab
    prevcolor = get(0,'defaultfigurecolor');
    set(0,'defaultfigurecolor',gui.myblack); %
    fig = figure('Visible','off',...
                 'MenuBar','none',...
                 'ToolBar','figure',...
                 'Color',gui.myblack,...
                 'Position',[200,200,gui.figX,gui.figY]);
    set(0,'defaultfigurecolor',prevcolor);
    %get(fig)
    % custom pointer? https://uk.mathworks.com/help/matlab/ref/figure-properties.html#zmw57dd0e277372
    
    p1dx = 0.28-0.005;
    p1dy = 1-0.005;
    
    %% TABS 
    gui.tabgp(1) = uitabgroup(fig,'Position',[0.005,0.005,1-p1dx-0.24,p1dy]);
    gui.tabs(1) = uitab(gui.tabgp(1),'Title','Viewer');    
    
    gui.tabgp(2) = uitabgroup(fig,'Position',[1-p1dx-0.24+0.005,0.005,0.5,0.3]);
    gui.tabs(2) = uitab(gui.tabgp(2),'Title','Main');
    
    gui.tabgp(3) = uitabgroup(fig,'Position',[1-p1dx-0.24+0.005,0.31+0.005,0.5,0.69]);
    gui.tabs(3) = uitab(gui.tabgp(3),'Title','Options');
    
    %% Main Microscope-Control
    gui.panels(1) = uipanel(gui.tabs(2),'Title','Main','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,0.5]);
    gui.panels(2) = uipanel(gui.tabs(2),'Title','Navigation','FontSize',12,...
                            'Position',[0.005,0.5+0.005,1-0.005,0.5]);
    
    %% Microscope-Control View Panels    
    gui.panels(3) = uipanel(gui.tabs(1),'Title','Image','FontSize',12,...
                            'Position',[0.005,0.15+0.005,1-0.005,0.85-0.005]);    
    
    %% 
    gui.panels(4) = uipanel(gui.tabs(3),'Title','','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);    
    
    %% Mains
    labelstrs = {'Open folder','Open set','Save','Quit'};
    for i = [1:4]
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1]);
    end
    set(gui.controls(1+lastc),'Callback',@HloadFolderbutton_Callback);
    set(gui.controls(2+lastc),'Callback',@HloadSetbutton_Callback);
    set(gui.controls(3+lastc),'Callback',@Hsavebutton_Callback);
    set(gui.controls(4+lastc),'Callback',@Hquitbutton_Callback);
    lastc = 4+lastc;
    
    %% 
    labelstrs = {{'Microscope-Control','MEK-Metamorph','DL-Metamorph','MEK-Live'},{'Max project','Mean project'}};
    for i = 1:2
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','popup','String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2*bwidth1),3*dw1+bheight1,2*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.fileModepop = 1+lastc;
    gui.handleMap.projModepop = 2+lastc;
    set(gui.controls(2+lastc),'Value',2);
    set(gui.controls(2+lastc),'Callback',@updateIMGproj_Callback);
    %set(gui.controls(i+2),'Callback',@updateIMGproj_Callback);
    lastc = 2+lastc;
    
    %% Microscope-Control UIcontrol
    gui.controls(1+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','Load a folder with data',...
                                      'HorizontalAlignment','center',...
                                      'FontSize',14,...
                                      'Position',[dw1,bheight1-5,4*bwidth1,bheight1]);
    
    gui.controls(2+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','CHN',...
                                      'HorizontalAlignment','left',...
                                      'FontSize',12,...
                                      'Position',[(3-1)*(dw1+2.2*bwidth1)-10,bheight1-5,40,bheight1*0.75]);
    gui.controls(3+lastc) = uicontrol(gui.panels(2),'Style','popup','String',{''},...
                                      'Callback',@updateIMGpop_Callback,...
                                      'Position',[25+dw1+(3-1)*(dw1+2.2*bwidth1),bheight1,1.9*bwidth1,bheight1*0.75]);
    gui.handleMap.('PrefixTitle') = 1+lastc;
    gui.handleMap.('CHpopup')     = 3+lastc;
    lastc = 3+lastc;
    
    labelstrs = {'ST','XY','IT'};
    typestrs = {'text','text','text'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2.2*bwidth1),dw1-5,25,bheight1*0.75]);
    end
    lastc = 3+lastc;
    
    labelstrs = {{''},{''},{''}};
    typestrs = {'popup','popup','popup'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Callback',@updateIMGpop_Callback,...
                                          'Position',[25+dw1+(i-1)*(dw1+2.2*bwidth1),dw1,1.9*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.('STpopup') = 1+lastc;
    gui.handleMap.('XYpopup') = 2+lastc;
    gui.handleMap.('ITpopup') = 3+lastc;
    lastc = 3+lastc;

    %% IMG 
    img = zeros(512,512);
    gui.axes(1) = axes('Position',[0,0.01,0.95,0.95],'Parent',gui.panels(3));    
    gui.imgs    = imshow(img,[],'Parent',gui.axes(1));
    set(gui.imgs,'ButtonDownFcn',@MouseShowValue);
    gui.handleMap.imageAxes = 1;
    
    gui.controls(1+lastc) = uicontrol(gui.panels(3),'Style','text',...
                                      'String','Click on image',...
                                      'FontSize',12,...
                                      'Position',[0,570,600,bheight1*0.5]);
    gui.handleMap.imageValue = 1+lastc;
    lastc = 1+lastc;
    
    %% IMG navigation
    labelstrs = {'IT>','IT<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,100+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpIT);
    set(gui.controls(2+lastc),'Callback',@moveDwIT);
    lastc = 2+lastc;
    labelstrs = {'CH>','CH<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,200+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpCH);
    set(gui.controls(2+lastc),'Callback',@moveDwCH);
    lastc = 2+lastc;
    labelstrs = {'XY>','XY<'};
    typestrs = {'pushbutton','pushbutton'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[567.5,300+i*(25+5),40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveUpXY);
    set(gui.controls(2+lastc),'Callback',@moveDwXY);
    lastc = 2+lastc;
    
    %%
    columnNames = {'XY','Use?'};
    tableData = {'',false};
    gui.tables(1) = uitable(gui.panels(4),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true],...
                            'Position',[dw1,dw1,4*bwidth1,12*(bheight1)]);
    gui.handleMap.XYuse = 1;
    
    labelstrs = {{''},{''}};
    typestrs = {'popup','popup'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(4),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+4*bwidth1+(i-1)*(dw1+1.4*bwidth1),400,1.4*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.ITinitpop = 1+lastc;
    gui.handleMap.ITendpop  = 2+lastc;
    lastc = 2+lastc;
    
    labelstrs = {'Init','End'};
    for i = [1:2]
        gui.controls(i+lastc) = uicontrol(gui.panels(4),'Style','text',...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+4*bwidth1+(i-1)*(dw1+1.4*bwidth1),430,bwidth1,bheight1*0.75]);
    end
    lastc = 2+lastc;
    
    %% Color and display
    for t = 1:length(gui.tabs)
        %set(gui.tabs(t),'ForegroundColor',[0.9,0.4,0]);%[0.6,0,0]
        set(gui.tabs(t),'BackgroundColor',gui.myblack);
    end
    for p = 1:length(gui.panels)
        set(gui.panels(p),'BackgroundColor',gui.myblack);
        % orange
        set(gui.panels(p),'ForegroundColor',[1,0.5,0.25]); %gui.mywhite
    end
    for c = 1:length(gui.controls)
        if(strcmp(get(gui.controls(c),'Style'),'text'))
            set(gui.controls(c),'BackgroundColor',gui.myblack);
            set(gui.controls(c),'ForegroundColor',gui.mywhite);
        else
            set(gui.controls(c),'BackgroundColor',gui.mywhite);
            set(gui.controls(c),'ForegroundColor',gui.myblack);
        end
    end
    
    %% Init
    set([fig,gui.axes,gui.panels,gui.tabs,gui.controls,gui.tables],'Units','normalized');
    set(fig,'Name','Cell Segmentation');
    set(fig,'Visible','on');
    movegui(fig,'center');
    
    function Hquitbutton_Callback(~,~)
        close(fig);
    end
    
    %% Microscope-control loading
    function [] = HloadFolderbutton_Callback(Hobj,~) 
        set(Hobj,'BackgroundColor',gui.busycolor);
        foldername = uigetdir();
        if(ischar(foldername))
            foldername = [foldername,'/'];
            if(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==1)
                %% MATLAB files, from Microscope-Control GUI
                imgfiles = dir([foldername,'IMG-*.mat']);
                logfiles = dir([foldername,'*LOG-*.mat']);
                %loadfiles(imgfiles,foldername,2);
                if(length(imgfiles)>0)
                    [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);            
                    gui.folder = foldername;
                    set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
                    if(~isempty(steps))
                        set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
                    end
                    xytable = {};
                    for xy = 1:length(xys)
                        xytable{xy,1} = xys{xy};
                        xytable{xy,2} = false;
                    end
                    set(gui.tables(gui.handleMap.('XYuse')),'Data',xytable);
                    set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);
                    set(gui.controls(gui.handleMap.('ITpopup')),'String',its);
                    set(gui.controls(gui.handleMap.('ITinitpop')),'String',its);
                    set(gui.controls(gui.handleMap.('ITendpop')),'String',its);
                    set(gui.controls(gui.handleMap.('ITendpop')),'Value',length(its));
                    set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
                    % ST x XY x CHN x IT
                    if(length(steps)==0)
                        nsteps = 1;
                        steps = {''};
                    else
                        nsteps = length(steps);
                    end
                    frameset = cell(nsteps,length(xys),length(chns),length(its));
                    for s = 1:length(steps)
                        for xy = 1:length(xys)
                            for ch = 1:length(chns)
                                for it = 1:length(its)
                                    if(~strcmp(steps{s},''))
                                        filename = ['IMG-',prefix,...
                                                    '_ST-',steps{s},...
                                                    '_XY-',xys{xy},...
                                                    '_CHN-',chns{ch},...
                                                    '_IT-',its{it},...
                                                    '.mat'];
                                    else
                                        filename = ['IMG-',prefix,...
                                                    '_XY-',xys{xy},...
                                                    '_CHN-',chns{ch},...
                                                    '_IT-',its{it},...
                                                    '.mat'];
                                    end
                                    filepath = [gui.folder,filename];
                                    if(~exist(filepath))
                                        error(['File does not exists: ',filepath]);
                                    end
                                    frame = struct;
                                    frame.prefix    = prefix;
                                    frame.step      = steps{s};
                                    frame.xy        = xys{xy};
                                    frame.chn       = chns{ch};
                                    frame.it        = its{it};
                                    frame.folder    = foldername;
                                    frame.filename  = filename;
                                    frame.filepath  = filepath;
                                    frameset{s,xy,ch,it} = frame;
                                end
                            end
                        end            
                    end
                    updateIMGpop_Callback();
                end
            elseif(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==2)
                %% TIF files, from Metamorph MEK
                imgfiles = dir([foldername,'*.TIF']);
            elseif(get(gui.controls(gui.handleMap.('fileModepop')),'Value')==3)
                %% TIF files, from Metamorph DL
                imgfiles = dir([foldername,'*.TIF']);
                if(length(imgfiles)>0)
                    [prefix,steps,xys,chns,its] = parseDLfiles(imgfiles);
                    gui.folder = foldername;
                    set(gui.controls(gui.handleMap.('PrefixTitle')),'String',prefix);
                    if(~isempty(steps))
                        set(gui.controls(gui.handleMap.('STpopup')),'String',steps);
                    end
                    if(~isempty(xys))
                        set(gui.controls(gui.handleMap.('XYpopup')),'String',xys);                        
                    end                    
                    set(gui.controls(gui.handleMap.('ITpopup')),'String',its);                    
                    set(gui.controls(gui.handleMap.('CHpopup')),'String',chns);
                    if(length(steps)==0)
                        nsteps = 1;
                        steps = {''};
                    else
                        nsteps = length(steps);
                    end
                    if(length(xys)==0)
                        nxys = 1;
                        xys  = {''};
                    else
                        nxys = length(xys);
                    end
                    % ST x XY x CHN x IT
                    frameset = cell(nsteps,nxys,length(chns),length(its));
                    for s = 1:length(steps)
                        for xy = 1:length(xys)
                            for ch = 1:length(chns)
                                for it = 1:length(its)
                                    % TODO: add stage support!
                                    filename = [prefix,...
                                                '_',chns{ch},...
                                                '_',its{it},...
                                                '.TIF'];
                                    filepath = [gui.folder,filename];
                                    if(~exist(filepath))
                                        error(['File does not exists: ',filepath]);
                                    end
                                    frame = struct;
                                    frame.prefix    = prefix;
                                    frame.step      = steps{s};
                                    frame.xy        = xys{xy};
                                    frame.chn       = chns{ch};
                                    frame.it        = its{it};
                                    frame.folder    = foldername;
                                    frame.filename  = filename;
                                    frame.filepath  = filepath;
                                    frameset{s,xy,ch,it} = frame;
                                end
                            end
                        end            
                    end
                    updateIMGpop_Callback();
                end
            else
                error(['Error while parsing folder']);
            end
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
        
    %% TODO: fix saving and loading
    function [] = Hsavebutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        prefix = get(gui.controls(gui.handleMap.('PrefixTitle')),'String');
        [filename,filepath] = uiputfile(['./',prefix,'_subset.mat'],'Save analysis');
        temp = frameset;
        xyTable = get(gui.tables(gui.handleMap.('XYuse')),'Data');
        useXY   = find(cell2mat(xyTable(:,2))==1)';
        if(~(filename==0))
            if(length(useXY)==0)
                return;
            end
            itinit  = get(gui.controls(gui.handleMap.('ITinitpop')),'Value');
            itend   = get(gui.controls(gui.handleMap.('ITendpop')),'Value');
            date   = getdate();
            fileMode    = get(gui.controls(gui.handleMap.fileModepop),'Value');
            projMode    = get(gui.controls(gui.handleMap.projModepop),'Value');
            prefix      = get(gui.controls(gui.handleMap.('PrefixTitle')),'String');
            steps = get(gui.controls(gui.handleMap.('STpopup')),'String');
            xys   = get(gui.controls(gui.handleMap.('XYpopup')),'String');
            its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
            chns  = get(gui.controls(gui.handleMap.('CHpopup')),'String');
            frameset  = frameset(:,useXY,:,itinit:itend);
            xys       = xys(useXY);
            its       = its(itinit:itend);
            save([filepath,filename],...
                 'frameset','date','lastversion',...
                 'prefix','steps','xys','its','chns');
        end
        frameset = temp;
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = HloadSetbutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uigetfile(['./','.mat'],'Load analysis');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            frameset = loadin.frameset;
            set(gui.controls(gui.handleMap.('PrefixTitle')),'String',loadin.prefix);
            set(gui.controls(gui.handleMap.('STpopup')),'String',loadin.steps);
            set(gui.controls(gui.handleMap.('XYpopup')),'String',loadin.xys);
            set(gui.controls(gui.handleMap.('ITpopup')),'String',loadin.its);
            set(gui.controls(gui.handleMap.('CHpopup')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('STpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',1);
            
            xytable = {};
            for xy = 1:length(loadin.xys)
                xytable{xy,1} = loadin.xys{xy};
                xytable{xy,2} = false;
            end
            set(gui.tables(gui.handleMap.('XYuse')),'Data',xytable);
            
            set(gui.controls(gui.handleMap.('ITinitpop')),'String',loadin.its);
            set(gui.controls(gui.handleMap.('ITendpop')),'String',loadin.its);
            set(gui.controls(gui.handleMap.('ITinitpop')),'Value',1);
            set(gui.controls(gui.handleMap.('ITendpop')),'Value',length(loadin.its));
            
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',1);
            if(isfield(loadin,'fileMode'))
                set(gui.controls(gui.handleMap.fileModepop),'Value',loadin.fileMode);
            end
            if(isfield(loadin,'projMode'))
                set(gui.controls(gui.handleMap.projModepop),'Value',loadin.projMode);
            end
            updateIMGpop_Callback();
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [frame,nst,nxy,nch,nit] = getcurrentframe()
        nst    = get(gui.controls(gui.handleMap.('STpopup')),'Value');
        nxy    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        nit    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        nch    = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        frame  = frameset{nst,nxy,nch,nit};
    end

    %% Image update
    function [] = updateIMGpop_Callback(~,~)
        [frame,nst,nxy,nch,nit] = getcurrentframe();
        [img] = loadimg(frame);
        cla(gui.axes(gui.handleMap.imageAxes));
        gui.imgs = imshow(img,[],'Parent',gui.axes(gui.handleMap.imageAxes));
        setMouseAction(gui.imgs);
    end 
    function [] = updateIMGproj_Callback(~,~)
        if(~isempty(frameset))
            updateIMGpop_Callback();
        end
    end
    function [img] = loadimg(frame)
        if(strcmp(frame.filepath(end-3:end),'.mat'))
            temp = load(frame.filepath);
            img = temp.img;
        else
            [stack,nstacks] = loadTIFstack(frame.filepath);
            [img] = stack2img(stack,nstacks);
        end
        img = double(img);
    end
    function [img] = stack2img(stack,nstacks)
        if(nstacks==1)
            img = stack(:,:,1);
        else
            projmode = get(gui.controls(gui.handleMap.('projModepop')),'Value');
            if(projmode==1)
                img = maxproject(stack);
            elseif(projmode==2)
                img = meanproject(stack);
            end
        end
    end
    
    %% Navigation for Microscope-Control
    function [] = moveUpIT(~,~)
        its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits   = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==length(its)))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==1))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==length(chns)))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==1))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys   = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==length(xys)))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==1))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys-1);
        end
        updateIMGpop_Callback();
    end

    %% Mouse capture
    function setMouseAction(imgh)
        set(imgh,'ButtonDownFcn',@MouseShowValue);
    end
    function [] = MouseShowValue(Hobj,event)
        [x,y] = getPointerPos(event);
        img = get(Hobj,'Cdata');
        value = img(x,y);
        set(Hobj,'Cdata',img);
        str = ['x:',num2str(x),' ','y:',num2str(y),' ','int:',num2str(value)];
        set(gui.controls(gui.handleMap.imageValue),'String',str);
    end
    function [x,y] = getPointerPos(HitEvent)
        pos = HitEvent.IntersectionPoint;
        y = min([512,max([1,floor((512/510)*(pos(1)))])]);
        x = min([512,max([1,floor((512/510)*(pos(2)))])]);
    end

end