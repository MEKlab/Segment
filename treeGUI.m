function [] = treeGUI()

    lastversion = '20170830';
    
    currfolder = [fileparts(which('treeGUI')),'/'];
    addpath(genpath([currfolder,'/src/']));
    
    %% TODO
    %% -> remove lineages
    %% -> merge lineages
    %% view/edit childs
    %% view data?
    
    gui = struct;
    gui.myblack = 0.2*[1,1,1];
    gui.mywhite = 0.99*[1,1,1];
    gui.defbkgcol = [0.94,0.94,0.94];
    gui.busycolor = 'yellow';
    gui.OFFcolor  = 'red';
    gui.ONcolor   = 'green';
    
    gui.handleMap = struct;
    gui.folder    = '';
    gui.date = getdate();
    
    gui.figX = 1300;
    gui.figY = 720;
    
    Xoffset  = 0.005*gui.figX;
    Yoffset  = 0.005*gui.figY;
    dw1      = 5;
    bheight1 = 40;
    bwidth1   = 91;
    
    frameset = {};
    treeset  = {};
    
    lastc = 0;
    % https://uk.mathworks.com/matlabcentral/answers/96816-how-do-i-change-the-default-background-color-of-all-figure-objects-created-in-matlab
    prevcolor = get(0,'defaultfigurecolor');
    set(0,'defaultfigurecolor',gui.myblack); %
    fig = figure('Visible','off',...
                 'MenuBar','none',...
                 'ToolBar','figure',...
                 'Color',gui.myblack,...
                 'Position',[200,200,gui.figX,gui.figY]);
    set(0,'defaultfigurecolor',prevcolor);
    %get(fig)
    % custom pointer? https://uk.mathworks.com/help/matlab/ref/figure-properties.html#zmw57dd0e277372
    
    p1dx = 0.05-0.005;
    p1dy = 1-0.005;
    
    %% TABS 
    gui.tabgp(1) = uitabgroup(fig,'Position',[0.005,0.2+0.005,1-0.24,0.8-0.005]);
    gui.tabs(1)  = uitab(gui.tabgp(1),'Title','Imgs');    
    
    gui.tabgp(2) = uitabgroup(fig,'Position',[0.005,0.005,1-0.24,0.2-0.005]);
    gui.tabs(2)  = uitab(gui.tabgp(2),'Title','Main');
    
    gui.tabgp(3) = uitabgroup(fig,'Position',[1-0.24+0.005,0.005,0.24-0.005,1-0.005]);
    gui.tabs(3)  = uitab(gui.tabgp(3),'Title','');
    
    gui.tabs(4) = uitab(gui.tabgp(1),'Title','Graph');    
    gui.tabs(5) = uitab(gui.tabgp(1),'Title','Plots');    
    gui.tabs(6) = uitab(gui.tabgp(1),'Title','Zoom');    
    gui.tabs(7) = uitab(gui.tabgp(1),'Title','Population');    
    
    %% Main Microscope-Control
    gui.panels(1) = uipanel(gui.tabs(2),'Title','Main','FontSize',12,...
                            'Position',[0.66+0.005,0.005,0.33-0.005,1-0.005]);
    gui.panels(2) = uipanel(gui.tabs(2),'Title','Navigation','FontSize',12,...
                            'Position',[0.005,0.005,0.66-0.005,1-0.005]);
    
    %% Microscope-Control View Panels    
    gui.panels(3) = uipanel(gui.tabs(1),'Title','Image','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);    
    
    %% Edit Panels    
    gui.panels(4) = uipanel(gui.tabs(3),'Title','Edit','FontSize',12,...
                            'Position',[0.005,0.2+0.005,1-0.005,0.8-0.005]);   
    gui.panels(5) = uipanel(gui.tabs(3),'Title','Edit','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,0.2-0.005]);   
    
    %% Graph Panel
    gui.panels(6) = uipanel(gui.tabs(4),'Title','','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);
    %% Plot Panel
    gui.panels(7) = uipanel(gui.tabs(5),'Title','','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);
    %% Zoom Panel
    gui.panels(8) = uipanel(gui.tabs(6),'Title','','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);
    %% Population Panel
    gui.panels(9) = uipanel(gui.tabs(7),'Title','','FontSize',12,...
                            'Position',[0.005,0.005,1-0.005,1-0.005]);
    
    lastc = 0;
    %% Mains
    labelstrs = {'Open set','Save','Quit'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1]);
    end
    set(gui.controls(1+lastc),'Callback',@HloadSetbutton_Callback);
    set(gui.controls(2+lastc),'Callback',@Hsavebutton_Callback);
    set(gui.controls(3+lastc),'Callback',@Hquitbutton_Callback);
    lastc = 3+lastc;
    
    labelstrs = {{'Max project','Mean project'}};
    for i = 1:1
        gui.controls(i+lastc) = uicontrol(gui.panels(1),'Style','popup','String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2*bwidth1),4*dw1+bheight1,2*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.projModepop = 1+lastc;
    set(gui.controls(1+lastc),'Value',2);
    lastc = 1+lastc;
    
    %% Microscope-Control UIcontrol
    gui.controls(1+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','Load a folder with data',...
                                      'HorizontalAlignment','center',...
                                      'FontSize',14,...
                                      'Position',[dw1,bheight1,4*bwidth1,bheight1]);
    
    gui.controls(2+lastc) = uicontrol(gui.panels(2),'Style','text',...
                                      'String','CHN',...
                                      'HorizontalAlignment','left',...
                                      'FontSize',12,...
                                      'Position',[(3-1)*(dw1+2.2*bwidth1)-10,bheight1,40,bheight1*0.75]);
    gui.controls(3+lastc) = uicontrol(gui.panels(2),'Style','popup','String',{''},...
                                      'Callback',@updateIMGpop_Callback,...
                                      'Position',[25+dw1+(3-1)*(dw1+2.2*bwidth1),bheight1+5,1.9*bwidth1,bheight1*0.75]);
    gui.handleMap.('PrefixTitle') = 1+lastc;
    gui.handleMap.('CHpopup')     = 3+lastc;
    lastc = 3+lastc;
    
    labelstrs = {'ST','XY','IT'};
    typestrs = {'text','text','text'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[dw1+(i-1)*(dw1+2.2*bwidth1),dw1,25,bheight1*0.75]);
    end
    lastc = 3+lastc;
    
    labelstrs = {{''},{''},{''}};
    typestrs = {'popup','popup','popup'};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(2),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Callback',@updateIMGpop_Callback,...
                                          'Position',[25+dw1+(i-1)*(dw1+2.2*bwidth1),dw1+5,1.9*bwidth1,bheight1*0.75]);
    end
    gui.handleMap.('STpopup') = 1+lastc;
    gui.handleMap.('XYpopup') = 2+lastc;
    gui.handleMap.('ITpopup') = 3+lastc;
    set(gui.controls(2+lastc),'Callback',@updateIMGpop_Callback2);
    lastc = 3+lastc;
    
    %% IMGs 
    img = zeros(512,512);
    gui.axes(1) = axes('Position',[0,0.025,0.5,1],'Parent',gui.panels(3));    
    gui.axes(2) = axes('Position',[0.5+0.005,0.025,0.5,1],'Parent',gui.panels(3));        
    gui.imgs{1}  = imshow(img,[],'Parent',gui.axes(1));
    gui.imgs{2}  = imshow(img,[],'Parent',gui.axes(2));
    set(gui.imgs{1},'ButtonDownFcn',@MouseShowValue);
    set(gui.imgs{2},'ButtonDownFcn',@MouseShowValue);
    gui.handleMap.imageAxes1 = 1;
    gui.handleMap.imageAxes2 = 2;

    gui.axes(3) = axes('Position',[0.005,0.05,1,0.9],'Parent',gui.panels(6));    
    gui.imgs{3}  = [];
    gui.handleMap.GraphAxes  = 3;    
    
    gui.axes(4) = axes('Position',[0.08,0.04,0.9,0.4],'Parent',gui.panels(7));    
    gui.imgs{4} = [];
    gui.axes(5) = axes('Position',[0.08,0.47+0.08,0.9,0.4],'Parent',gui.panels(7));
    gui.imgs{5} = [];
    gui.handleMap.PlotsAxes1  = 5;
    gui.handleMap.PlotsAxes2  = 4;
    
    img = zeros(100,75);
    gui.handleMap.ZoomAxes = [];
    count = 0;
    for i = [2,1]
        for j = 1:6
            count = count+1;
            gui.axes(5+count) = axes('Position',[0.02+(j-1)*(0.15+0.01),0.1+(i-1)*(0.03+0.4),0.15,0.35],...
                                     'Parent',gui.panels(8));    
            gui.imgs{5+count} = imshow(img,[],'Parent',gui.axes(5+count));
            title(num2str(count),'Color',gui.mywhite);
            gui.handleMap.ZoomAxes(count) = 5+count;
        end
    end
    
    gui.axes(18) = axes('Position',[0.08,0.08,0.9,0.85],'Parent',gui.panels(9));    
    gui.imgs{18}  = [];
    gui.handleMap.PopulationAxes  = 18;

    gui.controls(1+lastc) = uicontrol(gui.panels(3),'Style','text',...
                                      'String','Click on image',...
                                      'FontSize',12,...
                                      'Position',[0,570,600,bheight1*0.5]);
    gui.handleMap.imageValue = 1+lastc;
    lastc = 1+lastc;
    
    %% IMG navigation
    labelstrs = {'XY<','XY>','CH<','CH>','IT<','IT>'};
    typestrs = {'pushbutton','pushbutton','pushbutton','pushbutton','pushbutton','pushbutton'};
    for i = [1:6]
        gui.controls(i+lastc) = uicontrol(gui.panels(3),'Style',typestrs{i},...
                                          'FontSize',12,...
                                          'String',labelstrs{i},...
                                          'Position',[299+i*(40+10),2.5,40,25]);
    end
    set(gui.controls(1+lastc),'Callback',@moveDwXY);
    set(gui.controls(2+lastc),'Callback',@moveUpXY);
    set(gui.controls(3+lastc),'Callback',@moveDwCH);
    set(gui.controls(4+lastc),'Callback',@moveUpCH);
    set(gui.controls(5+lastc),'Callback',@moveDwIT);
    set(gui.controls(6+lastc),'Callback',@moveUpIT);
    lastc = 6+lastc;

    %% Graph
    gui.controls(1+lastc) = uicontrol(gui.panels(6),'Style','pushbutton',...
                                      'FontSize',12,...
                                      'String','Update',...
                                      'Position',[299+3*(40+10),2.5,100,25]);
    set(gui.controls(1+lastc),'Callback',@UpdateGraph_CallBack);
    lastc = 1+lastc;
    gui.controls(1+lastc) = uicontrol(gui.panels(6),'Style','text',...
                                      'FontSize',14,...
                                      'String','Cell ID:',...
                                      'Position',[75,540,300,25]);
    gui.handleMap.GraphTxt   = 1+lastc;
    lastc = 1+lastc;
    
    %% Plots    
    gui.plotpopstr = {'frame','time','lenght','area',...
                      'width','intensity','der-intensity',...
                      'background',...
                      'intensity-background','der-intensity-bkg'};
    idists = [450,150,300,600];
    ilens  = [150,150,100,100];
    strs = {gui.plotpopstr,gui.plotpopstr,{''},{''}};
    for i = 1:4
        gui.controls(i+lastc) = uicontrol(gui.panels(7),'Style','popup',...
                                          'String',strs{i},...
                                          'Position',[idists(i)+100+10,540+2.5,ilens(i),20]);
    end
    gui.controls(5+lastc) = uicontrol(gui.panels(7),'Style','text',...
                                      'FontSize',14,...
                                      'String','Cell ID:',...
                                      'Position',[75,540+2.5,150,25]);
    gui.handleMap.XCHNpop1  = 3+lastc;
    gui.handleMap.YCHNpop1  = 4+lastc;
    gui.handleMap.XPlotpop1 = 2+lastc;
    gui.handleMap.YPlotpop1 = 1+lastc;
    gui.handleMap.PlotTxt   = 5+lastc;
    set(gui.controls(gui.handleMap.('YPlotpop1')),'Value',3);
    lastc = 5+lastc;
    
    for i = 1:4
        gui.controls(i+lastc) = uicontrol(gui.panels(7),'Style','popup',...
                                          'String',strs{i},...
                                          'Position',[idists(i)+100+10,250+2.5,ilens(i),20]);
    end
    gui.handleMap.XCHNpop2  = 3+lastc;
    gui.handleMap.YCHNpop2  = 4+lastc;
    gui.handleMap.XPlotpop2 = 2+lastc;
    gui.handleMap.YPlotpop2 = 1+lastc;
    set(gui.controls(gui.handleMap.('XPlotpop2')),'Value',2);
    set(gui.controls(gui.handleMap.('YPlotpop2')),'Value',3);
    lastc = 4+lastc;
        
    gui.controls(1+lastc) = uicontrol(gui.panels(7),'Style','pushbutton',...
                                      'FontSize',12,...
                                      'String','Update',...
                                      'Position',[860,250+2.5,100,25]);
    set(gui.controls(1+lastc),'Callback',@UpdateTimePlot_CallBack);
    lastc = 1+lastc;
    
    %% Zoom
    idists = [50,250,450,650];
    ilengs = [100,100,100,250];
    for i = 1:4
        gui.controls(i+lastc) = uicontrol(gui.panels(8),'Style','popup',...
                                          'String',{''},...
                                          'Position',[idists(i)+20,540+2.5,ilengs(i),20],...
                                          'Callback',@ZoomPopup_CallBack);
    end
    set(gui.controls(2+lastc),'Callback',@ZoomPopup_CallBack2);
    set(gui.controls(4+lastc),'String',{'border','length'});
    gui.handleMap.ZoomCHNpop   = 1+lastc;
    gui.handleMap.ZoomCellpop  = 2+lastc;
    gui.handleMap.ZoomIterpop  = 3+lastc;
    gui.handleMap.ZoomModepop  = 4+lastc;
    lastc = 4+lastc;
    
    idists = [0,190,390,590];
    strs   = {'CHN','Cell ID','Frame','Extra'};
    for i = 1:4
        gui.controls(i+lastc) = uicontrol(gui.panels(8),'Style','text',...
                                          'FontSize',12,...
                                          'String',strs{i},...
                                          'Position',[idists(i),535,65,25]);
    end
    lastc = 4+lastc;
    
    gui.controls(1+lastc) = uicontrol(gui.panels(8),'Style','text',...
                                      'FontSize',14,...
                                      'String','Click on img',...
                                      'Position',[75,10,200,25]);
    gui.handleMap.imageValueZoom   = 1+lastc;
    lastc = 1+lastc;
    %gui.handleMap.imageValueZoom
    
    %% Population
    gui.controls(1+lastc) = uicontrol(gui.panels(9),'Style','pushbutton',...
                                      'FontSize',12,...
                                      'String','Update',...
                                      'Position',[860,530+2.5,100,25]);
    set(gui.controls(1+lastc),'Callback',@UpdatePopulationPlot_CallBack);
    lastc = 1+lastc;
    

    gui.popupopstr = {'lenght','area',...
                      'width','intensity',...
                      'lenght birth','lenght div','lenght delta',...
                      'generation time'};
    gui.controls(1+lastc) = uicontrol(gui.panels(9),'Style','popup',...
                                      'String',gui.popupopstr,...
                                      'Position',[530,530+2.5,200,25]);
    gui.controls(2+lastc) = uicontrol(gui.panels(9),'Style','popup',...
                                      'String',{''},...
                                      'Position',[740,530+2.5,100,25]);
    gui.handleMap.Popupop    = 1+lastc;
    gui.handleMap.PopuCHNpop = 2+lastc;
    lastc = 2+lastc;
    
    %% Edit
    columnNames = {'ID','Show','Parent','1st','End'};
    tableData = {'O',false,'0','0','0'};
    gui.tables(1) = uitable(gui.panels(4),'Data',tableData,...
                            'ColumnName',columnNames,'ColumnEditable',[false,true,true,true,true,true],...
                            'Position',[1*dw1,dw1,3.1*bwidth1,12.8*(bheight1)]);
    set(gui.tables(1),'ColumnWidth',{30,40,40,40,40});
    gui.handleMap.('TreeTable') = 1;
    
    labelstrs = {'Show All','Show None',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(dw1+bwidth1),dw1,bwidth1,bheight1*0.8]);
    end
    set(gui.controls(1+lastc),'Callback',@SetShowAll_Callback);
    set(gui.controls(2+lastc),'Callback',@SetShowNone_Callback);
    
    lastc = 3+lastc;
    labelstrs = {'','',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(dw1+bwidth1),2*dw1+bheight1*0.8,bwidth1,bheight1*0.8]);
    end
    lastc = 3+lastc;
    labelstrs = {'','',''};
    for i = [1:3]
        gui.controls(i+lastc) = uicontrol(gui.panels(5),'Style','pushbutton',...
                                          'String',labelstrs{i},...
                                          'Position',[2*dw1+(i-1)*(dw1+bwidth1),3*dw1+2*bheight1*0.8,bwidth1,bheight1*0.8]);
    end
% $$$     set(gui.controls(1+lastc),'Callback',@HloadSetbutton_Callback);
% $$$     set(gui.controls(2+lastc),'Callback',@Hsavebutton_Callback);
% $$$     set(gui.controls(3+lastc),'Callback',@Hquitbutton_Callback);
    lastc = 3+lastc;

    %% Color and display
    for t = 1:length(gui.tabs)
        %set(gui.tabs(t),'ForegroundColor',[0.9,0.4,0]);%[0.6,0,0]
        set(gui.tabs(t),'BackgroundColor',gui.myblack);
    end
    for p = 1:length(gui.panels)
        set(gui.panels(p),'BackgroundColor',gui.myblack);
        % orange
        set(gui.panels(p),'ForegroundColor',[1,0.5,0.25]); %gui.mywhite
    end
    set(gui.panels(7),'BackgroundColor',gui.mywhite);
    set(gui.panels(9),'BackgroundColor',gui.mywhite);
    for c = 1:length(gui.controls)
        if(strcmp(get(gui.controls(c),'Style'),'text'))
            set(gui.controls(c),'BackgroundColor',gui.myblack);
            set(gui.controls(c),'ForegroundColor',gui.mywhite);
        else
            set(gui.controls(c),'BackgroundColor',gui.mywhite);
            set(gui.controls(c),'ForegroundColor',gui.myblack);
        end
    end
    set(gui.controls(gui.handleMap.PlotTxt),'BackgroundColor',gui.mywhite);
    set(gui.controls(gui.handleMap.PlotTxt),'ForegroundColor',gui.myblack);
    
    %% Init ,gui.tables
    set([fig,gui.axes,gui.panels,gui.tabs,gui.controls],'Units','normalized');
    set(fig,'Name','Cell Segmentation');
    set(fig,'Visible','on');
    movegui(fig,'center');
    
    function Hquitbutton_Callback(~,~)
        close(fig);
    end
    
    %% Microscope-control loading
        
    %% TODO: fix saving and loading
    function [] = Hsavebutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uiputfile(['./',prefix,'_tree.mat'],'Save analysis');
        if(~(filename==0))            
            date   = getdate();
            save([filepath,filename],'date');
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = HloadSetbutton_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [filename,filepath] = uigetfile(['./','.mat'],'Load analysis');
        if(~(filename==0))
            loadin = load([filepath,filename]);
            frameset = loadin.frameset;
            treeset  = loadin.treeset;
            if(~isfield(treeset,'cellcolors'))
                assigncellcolors();
            end
            set(gui.controls(gui.handleMap.('PrefixTitle')),'String',loadin.prefix);
            set(gui.controls(gui.handleMap.('STpopup')),'String',loadin.steps);
            set(gui.controls(gui.handleMap.('XYpopup')),'String',loadin.xys);
            set(gui.controls(gui.handleMap.('ITpopup')),'String',loadin.its);
            set(gui.controls(gui.handleMap.('CHpopup')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('ZoomCHNpop')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('PopuCHNpop')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('XCHNpop1')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('YCHNpop1')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('XCHNpop2')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('YCHNpop2')),'String',loadin.chns);
            set(gui.controls(gui.handleMap.('STpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',1);
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',1);
            updateTreeTable();
            updateIMGpop_Callback();
        end
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [frame,nst,nxy,nch,nit] = getcurrentframe()
        nst    = get(gui.controls(gui.handleMap.('STpopup')),'Value');
        nxy    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        nit    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        nch    = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        frame  = frameset{nst,nxy,nch,nit};
    end
    
    %% Tree update
    function [] = updateTreeTable()        
        [~,nst,nxy,~,~] = getcurrentframe();
        tree   = treeset{nst,nxy};
        ucells = tree.ucells;
        colors  = tree.cellcolor;
        ttable  = {};
        strs = {};
        for uc = 1:ucells
            strs{uc} = num2str(uc);
            cell = tree.cells{uc};
            idstr = colergen(cellstr(rgb2hex(colors(uc,:))),num2str(uc)); 
            if(cell.parent>0)
                prstr = colergen(cellstr(rgb2hex(colors(cell.parent,:))),num2str(cell.parent)); 
            else
                prstr = '0';
            end
            %childs{uc}
            ttable(uc,:) = {idstr,true,prstr,num2str(cell.inframes(1)),num2str(cell.inframes(end))};
        end
        set(gui.tables(gui.handleMap.('TreeTable')),'Data',ttable);
        % update list of cells in Zoom
        set(gui.controls(gui.handleMap.('ZoomCellpop')),'String',strs);
    end
    function [str] = colergen(color,text) 
        str  = char(strcat('<html><table border=0 width=400 bgcolor=',...
                           color,'><TR><TD>',...
                           text,'</TD></TR> </table></html>'));
    end
    function [] = assigncellcolors()
        [nst,nxy] = size(treeset);
        for st = 1:nst
            for xy = 1:nxy
                tree = treeset{st,xy};
                colors = zeros(tree.ucells,3);
                for u = 1:tree.ucells
                    colors(u,:) = randcolor();
                end
                treeset{st,xy}.cellcolor = colors;
            end
        end
    end
    
    function [] = SetShowAll_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [~,nst,nxy,~,~] = getcurrentframe();
        ttable = get(gui.tables(gui.handleMap.('TreeTable')),'Data');
        ucells = treeset{nst,nxy}.ucells;
        for c = 1:ucells
            ttable{c,2} = true;
        end
        set(gui.tables(gui.handleMap.('TreeTable')),'Data',ttable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = SetShowNone_Callback(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [~,nst,nxy,~,~] = getcurrentframe();
        ttable = get(gui.tables(gui.handleMap.('TreeTable')),'Data');
        ucells = treeset{nst,nxy}.ucells;
        for c = 1:ucells
            ttable{c,2} = false;
        end
        set(gui.tables(gui.handleMap.('TreeTable')),'Data',ttable);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end

    %% Graph Update
    function [] = UpdateGraph_CallBack(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        [~,nst,nxy,~,~] = getcurrentframe();
        colors  = treeset{nst,nxy}.cellcolor;
        ucells2 = treeset{nst,nxy}.ucells2;
        cells2  = treeset{nst,nxy}.cells2;
        [index,starts,ends] = ordertree(cells2);
    % $$$         graph = treeset.graphs{nst,nxy};
        axes(gui.axes(gui.handleMap.GraphAxes));
        cla(gui.axes(gui.handleMap.GraphAxes));
        hold on;
        for c = 1:ucells2
            id = cells2{c}.ID;
            if(~isempty(cells2{c}.childs))
                childs = cells2{c}.childs;
                plot([index(childs(1));index(childs(2))],-1*[starts(childs(1));starts(childs(2))],'-','Color','black');
            end
            plot([index(c);index(c)],-1*[starts(c);ends(c)+1],'-','Color',colors(id,:));
            p = plot(index(c),-1*starts(c),'o','Color',colors(id,:),'MarkerSize',5);
            set(p,'UserData',[id,c]);
            set(p,'ButtonDownFcn',@selectCellGraphLine);
        end
        axis([-10 ucells2+10 -1*(max(ends)+10) 10]);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    
    function [] = UpdateTimePlot_CallBack(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        
        Xplotpop1 = get(gui.controls(gui.handleMap.('XPlotpop1')),'Value');
        Yplotpop1 = get(gui.controls(gui.handleMap.('YPlotpop1')),'Value');
        XCHNpop1 = get(gui.controls(gui.handleMap.('XCHNpop1')),'Value');
        YCHNpop1 = get(gui.controls(gui.handleMap.('YCHNpop1')),'Value');
        Xplotpop2 = get(gui.controls(gui.handleMap.('XPlotpop2')),'Value');
        Yplotpop2 = get(gui.controls(gui.handleMap.('YPlotpop2')),'Value');
        XCHNpop2 = get(gui.controls(gui.handleMap.('XCHNpop2')),'Value');
        YCHNpop2 = get(gui.controls(gui.handleMap.('YCHNpop2')),'Value');
        
        UpdateTimePlotAux(gui.handleMap.PlotsAxes1,Xplotpop1,Yplotpop1,XCHNpop1,YCHNpop1);
        UpdateTimePlotAux(gui.handleMap.PlotsAxes2,Xplotpop2,Yplotpop2,XCHNpop2,YCHNpop2);
        
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = UpdateTimePlotAux(PlotsAxes,Xplotpop,Yplotpop,XCHNpop,YCHNpop)
        [~,nst,nxy,~,~] = getcurrentframe();
        ttable = get(gui.tables(gui.handleMap.('TreeTable')),'Data');
        ucells = treeset{nst,nxy}.ucells;
        colors = treeset{nst,nxy}.cellcolor;
        axes(gui.axes(PlotsAxes));
        cla(gui.axes(PlotsAxes));
        [~,Xlabel,Xlogp] = getplotpopvars(Xplotpop,XCHNpop,treeset{nst,nxy}.cells{1});
        [~,Ylabel,Ylogp] = getplotpopvars(Yplotpop,YCHNpop,treeset{nst,nxy}.cells{1});
        if(Xlogp>0) set(gca,'XScale','log'); else set(gca,'XScale','linear'); end
        if(Ylogp>0) set(gca,'YScale','log'); else set(gca,'YScale','linear'); end
        xlabel(Xlabel);
        ylabel(Ylabel);
        hold on;
        for c = 1:ucells
            show   = ttable{c,2};
            if(show)
                Xvars = getplotpopvars(Xplotpop,XCHNpop,treeset{nst,nxy}.cells{c});
                Yvars = getplotpopvars(Yplotpop,YCHNpop,treeset{nst,nxy}.cells{c});
                p = plot(Xvars(1:length(Yvars)),Yvars,'Color',colors(c,:),'LineWidth',1);
                set(p,'UserData',c);
                set(p,'ButtonDownFcn',@selectCellPlotLine);
            end
        end
    end
    function [vars,label,logp] = getplotpopvars(plotpop,CHNpop,cell)
        plotpopstr = gui.plotpopstr{plotpop};
        logp  = 0;
        switch plotpopstr
          case 'time'
            vars = cell.times;
            label = 'Time (min)';
          case 'frame'
            vars = cell.inframes;
            label = 'Frame';
          case 'lenght'
            vars  = cell.lengths;
            label = 'Length (pixels)';
            logp  = 1;
          case 'der-lenght'
            vars = [];
            % TODO
            label = 'Der Length (pixels)';
          case 'area'
            vars = cell.areas;
            label = 'Area (pixels)';
            logp  = 1;
          case 'width'
            vars = cell.widths;
            label = 'Width (pixels)'; 
            logp  = 0;
          case 'intensity'
            vars  = cell.fluos(:,CHNpop);
            label = ['Intensity CHN ',num2str(CHNpop)]; 
          case 'der-intensity'
            svals = smooth(cell.fluos(:,CHNpop));
            vars  = diff(svals)./svals(2:end);
            label = ['Der Intensity CHN ',num2str(CHNpop)]; 
          case 'background'
            vars = cell.bkgs;
            label = ['Background CHN ',num2str(CHNpop)]; 
          case 'intensity-background'
            vars = cell.fluos-cell.bkgs;
            label = ['Int-Bkg CHN ',num2str(CHNpop)]; 
          case 'der-intensity-bkg'
            svals = smooth(cell.fluos-cell.bkgs);
            vars  = diff(svals)./svals(2:end);
            label = ['Der Int-Bkg CHN ',num2str(CHNpop)]; 
        end  
    end
    
    %% Zoom update
    function [] = ZoomPopup_CallBack2(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        ZoomCellpop = get(gui.controls(gui.handleMap.('ZoomCellpop')),'Value');
        [~,nst,nxy,~,~] = getcurrentframe();
        cell = treeset{nst,nxy}.cells{ZoomCellpop};
        strs = {};
        for n = 1:length(cell.inframes);
            strs{n} = num2str(cell.inframes(n));
        end
        set(gui.controls(gui.handleMap.('ZoomIterpop')),'String',strs);
        set(gui.controls(gui.handleMap.('ZoomIterpop')),'Value',1);
        UpdateZoom();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = ZoomPopup_CallBack(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        UpdateZoom();
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [] = UpdateZoom()
        ZoomCHNpop  = get(gui.controls(gui.handleMap.('ZoomCHNpop')),'Value');
        ZoomCellpop = get(gui.controls(gui.handleMap.('ZoomCellpop')),'Value');
        ZoomIterpop = get(gui.controls(gui.handleMap.('ZoomIterpop')),'Value');
        ZoomModepop = get(gui.controls(gui.handleMap.('ZoomModepop')),'Value');
        [~,nst,nxy,~,~] = getcurrentframe();
        color  = treeset{nst,nxy}.cellcolor(ZoomCellpop,:);
        cells  = treeset{nst,nxy}.cells{ZoomCellpop};
        % Max frames are 12
        initf  = min([length(cells.inframes),ZoomIterpop]);
        endf   = min([length(cells.inframes),ZoomIterpop+11]);
        dy = min([100,30+max(cells.lengths(initf:endf))]);
        count  = 0;
        for n = 1:length(initf:endf)
            count = count+1;
            it    = cells.inframes(initf)+n-1;
            axes(gui.axes(gui.handleMap.ZoomAxes(n)));
            cla(gui.axes(gui.handleMap.ZoomAxes(n)));
            hold on;
            
            posc = cells.center(n,:);
            dpos = int16(max([1,floor(posc(2)-dy/2)]));
            upos = int16(min([512,dpos+dy]));
            lpos = int16(max([1,floor(posc(1)-25)]));
            rpos = int16(min([512,lpos+50]));
            
            [img] = loadimg(frameset{nst,nxy,ZoomCHNpop,it});
            img   = img(dpos:upos,lpos:rpos);
            imgh = imshow(img,[],'Parent',gui.axes(gui.handleMap.ZoomAxes(n)));
            set(imgh,'ButtonDownFcn',@MouseShowValueZoom);
            
            fcell = frameset{nst,nxy,3,it}.cells{cells.inframesIndex(initf+n-1)};
            if(ZoomModepop==1) % border
                plot(fcell.border(:,2)-double(lpos)+1,fcell.border(:,1)-double(dpos)+1,'Color',color,'LineWidth',2);
            elseif(ZoomModepop==2) % length
                len = fcell.length;
                ang = (pi/2)*fcell.angle/90;
                cent = [posc(1)-double(lpos)+1,posc(2)-double(dpos)+1];
                ucor = cent+[cos(ang)*len/2,abs(sin(ang)*len/2)];
                dcor = cent-[cos(ang)*len/2,abs(sin(ang)*len/2)];
                plot(cent(1),cent(2),'s','Color',color,'MarkerSize',5,'LineWidth',2);
                plot([ucor(1),dcor(1)],[ucor(2),dcor(2)],'Color',color,'LineWidth',2);
            end
            title(num2str(it),'Color',gui.mywhite);
        end
        for n = (count+1):12
            if(n==count+1)
                it = initf+n-1;
                [img] = loadimg(frameset{nst,nxy,ZoomCHNpop,it});
                img   = img(dpos:upos,lpos:rpos);
                axes(gui.axes(gui.handleMap.ZoomAxes(n)));
                cla(gui.axes(gui.handleMap.ZoomAxes(n)));
                imgh = imshow(img,[],'Parent',gui.axes(gui.handleMap.ZoomAxes(n)));
                set(imgh,'ButtonDownFcn',@MouseShowValueZoom);
                title(num2str(it),'Color',gui.mywhite);
            else
                axes(gui.axes(gui.handleMap.ZoomAxes(n)));
                cla(gui.axes(gui.handleMap.ZoomAxes(n)));
                imshow(zeros(100,75),[],'Parent',gui.axes(gui.handleMap.ZoomAxes(n)));
                title('','Color',gui.mywhite);
            end
        end
    end
       
    %% Population plot update
    function [] = UpdatePopulationPlot_CallBack(Hobj,~)
        set(Hobj,'BackgroundColor',gui.busycolor);
        Popupop    = get(gui.controls(gui.handleMap.('Popupop')),'Value');
        PopuCHNpop = get(gui.controls(gui.handleMap.('PopuCHNpop')),'Value');
        [~,~,~,nits] = size(frameset);
        [~,nst,nxy,~,~] = getcurrentframe();
        axes(gui.axes(gui.handleMap.PopulationAxes));
        cla(gui.axes(gui.handleMap.PopulationAxes));
        data = getDataMatrix(treeset{nst,nxy}.cells2,nits,gui.popupopstr{Popupop},PopuCHNpop);
        hold on;
        mvals = zeros(1,nits);
        p25   = zeros(1,nits);
        p75   = zeros(1,nits);
        for it = 1:nits
            inframe = find(data(:,it)>0);
            vals = data(inframe,it)';
            mvals(it) = mean(vals);
            p25(it)   = prctile(vals,25);
            p75(it)   = prctile(vals,75);
            plot(it.*ones(size(vals)),vals,'.b');
        end
        xs = 1:nits;
        ppos = find(isnan(p75)==0);
        plot(1:nits,mvals,'r');
        myupdownfill(xs(ppos),p75(ppos),p25(ppos),'r',0.4,0);
        set(Hobj,'BackgroundColor',gui.defbkgcol);
    end
    function [data] = getDataMatrix(cells,nits,datastr,PopuCHNpop)
        ncells = length(cells);
        data = zeros(ncells,nits);
        for c = 1:ncells
            switch datastr
              case 'lenght'
                data(c,cells{c}.inframes) = cells{c}.lengths;
              case 'area'
                data(c,cells{c}.inframes) = cells{c}.areas;
              case 'width'
                data(c,cells{c}.inframes) = cells{c}.widths;
              case 'intensity'
                data(c,cells{c}.inframes) = cells{c}.fluos(:,PopuCHNpop);
              case 'lenght birth'
                if(~cells{c}.parent==0)
                    data(c,cells{c}.inframes) = cells{c}.lengths(1)*ones(size(cells{c}.inframes));
                else
                    data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
                end
              case 'lenght div'
                if(~cells{c}.divisions==0)
                    data(c,cells{c}.inframes) = cells{c}.lengths(end)*ones(size(cells{c}.inframes));
                else
                    data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
                end
              case 'lenght delta'
                if(~cells{c}.divisions==0 && ~cells{c}.parent==0)
                    data(c,cells{c}.inframes) = (cells{c}.lengths(end)-cells{c}.lengths(1))*ones(size(cells{c}.inframes));
                else
                    data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
                end
              case 'generation time'
                if(~cells{c}.divisions==0 && ~cells{c}.parent==0)
                    data(c,cells{c}.inframes) = (cells{c}.times(end)-cells{c}.times(1))*ones(size(cells{c}.inframes));
                else
                    data(c,cells{c}.inframes) = 0*ones(size(cells{c}.inframes));
                end
            end
        end
    end
    %% Image update
    function [] = updateIMGpop_Callback2(~,~)
        [~,nst,nxy,nch,nit] = getcurrentframe();
        updateTreeTable();
        updateIMGaxes(gui.handleMap.imageAxes1,frameset{nst,nxy,nch,nit});
        updateIMGaxes(gui.handleMap.imageAxes2,frameset{nst,nxy,nch,nit+1});
    end
    function [] = updateIMGpop_Callback(~,~)
        [~,nst,nxy,nch,nit] = getcurrentframe();
        updateIMGaxes(gui.handleMap.imageAxes1,frameset{nst,nxy,nch,nit});
        updateIMGaxes(gui.handleMap.imageAxes2,frameset{nst,nxy,nch,nit+1});
    end 
    function [] = updateIMGaxes(imgax,frame)
        [img] = loadimg(frame);
        cla(gui.axes(imgax));
        gui.imgs{imgax} = imshow(img,[],'Parent',gui.axes(imgax));
        axes(gui.axes(imgax));
        updateCells(imgax,frame);
        setMouseAction(gui.imgs{imgax});
    end
    function [] = updateIMGproj_Callback(~,~)
        if(~isempty(frameset))
            updateIMGpop_Callback();
        end
    end
    function [img] = loadimg(frame)
        if(strcmp(frame.filepath(end-3:end),'.mat'))
            temp = load(frame.filepath);
            img = temp.img;
        else
            [stack,nstacks] = loadTIFstack(frame.filepath);
            [img] = stack2img(stack,nstacks);
        end
        img = double(img);
    end
    function [img] = stack2img(stack,nstacks)
        if(nstacks==1)
            img = stack(:,:,1);
        else
            projmode = get(gui.controls(gui.handleMap.('projModepop')),'Value');
            if(projmode==1)
                img = maxproject(stack);
            elseif(projmode==2)
                img = meanproject(stack);
            end
        end
    end
    function [] = updateCells(imgax,frame)
        [~,nst,nxy,~,~] = getcurrentframe();
        colors = treeset{nst,nxy}.cellcolor;
        %parents = treeset.parents{nst,nxy};
        ttable = get(gui.tables(gui.handleMap.('TreeTable')),'Data');
        if(isfield(frame,'cells') && isfield(frame,'cellsIDs'))
            cells    = frame.cells;
            hold on;
            for i = 1:length(cells)
                border = cells{i}.border;
                center = cells{i}.center;
                cID    = frame.cellsIDs(i);
                show   = ttable{cID,2};
                if(show)
                    color  = colors(cID,:);
                    text(center(1)+5,center(2),...
                         [num2str(cID)],...
                         'FontSize',8,... 
                         'Color',color);%,'.',num2str(parents(cID))
                    h = plot(border(:,2),border(:,1),'Color',color,'LineWidth',1);
                    h.UserData = [imgax,i];
                    set(h,'ButtonDownFcn',@selectCell);
                end
            end
        end
    end
    
    %% Navigation for Microscope-Control
    function [] = moveUpIT(~,~)
        its   = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits   = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==length(its)))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwIT(~,~)
        its    = get(gui.controls(gui.handleMap.('ITpopup')),'String');
        nits    = get(gui.controls(gui.handleMap.('ITpopup')),'Value');
        if(~(nits==1))
            set(gui.controls(gui.handleMap.('ITpopup')),'Value',nits-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==length(chns)))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns+1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveDwCH(~,~)
        chns   = get(gui.controls(gui.handleMap.('CHpopup')),'String');
        nchns  = get(gui.controls(gui.handleMap.('CHpopup')),'Value');
        if(~(nchns==1))
            set(gui.controls(gui.handleMap.('CHpopup')),'Value',nchns-1);
        end
        updateIMGpop_Callback();
    end
    function [] = moveUpXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys   = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==length(xys)))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys+1);
        end
        updateTreeTable();
        updateIMGpop_Callback();
    end
    function [] = moveDwXY(~,~)
        xys    = get(gui.controls(gui.handleMap.('XYpopup')),'String');
        nxys    = get(gui.controls(gui.handleMap.('XYpopup')),'Value');
        if(~(nxys==1))
            set(gui.controls(gui.handleMap.('XYpopup')),'Value',nxys-1);
        end
        updateTreeTable();
        updateIMGpop_Callback();
    end

    %% Mouse capture
    function setMouseAction(imgh)
        set(imgh,'ButtonDownFcn',@MouseShowValue);
    end
    function [] = MouseShowValue(Hobj,event)
        [x,y] = getPointerPos(event);
        img = get(Hobj,'Cdata');
        value = img(x,y);
        %set(Hobj,'Cdata',img);
        str = ['x:',num2str(x),' ','y:',num2str(y),' ','int:',num2str(value)];
        set(gui.controls(gui.handleMap.imageValue),'String',str);
    end
    function [] = MouseShowValueZoom(Hobj,event)
        [x,y] = getPointerPos(event);
        img   = get(Hobj,'Cdata');
        value = img(x,y);
        %set(Hobj,'Cdata',img);
        str = ['x:',num2str(x),' ','y:',num2str(y),' ','int:',num2str(value)];
        set(gui.controls(gui.handleMap.imageValueZoom),'String',str);
    end
    function [x,y] = getPointerPos(HitEvent)
        pos = HitEvent.IntersectionPoint;
        y = min([512,max([1,floor((512/510)*(pos(1)))])]);
        x = min([512,max([1,floor((512/510)*(pos(2)))])]);
    end
    function selectCellPlotLine(Hobj,~)
        udata = get(Hobj,'UserData');
        set(gui.controls(gui.handleMap.PlotTxt),'String',['Cell ID ',num2str(udata)]);
    end
    function selectCellGraphLine(Hobj,~)
        udata = get(Hobj,'UserData');
        set(gui.controls(gui.handleMap.GraphTxt),'String',...
                          ['Cell ID ',num2str(udata(1)),' - ',num2str(udata(2))]);
    end
    function selectCell(Hobj,~)
        color   = get(Hobj,'Color');
        udata = get(Hobj,'UserData');
        imgax   = udata(1);
        cellind = udata(2);
        [~,nst,nxy,nch,nit] = getcurrentframe();
        frame = frameset{nst,nxy,nch,nit+imgax-1}; % plus 0 or 1
        colors = treeset.cellcolor{nst,nxy};
        ttable = get(gui.tables(gui.handleMap.('TreeTable')),'Data');
        cID = frame.cellsIDs(cellind);
        if(color(2)==0)
            set(Hobj,'Color',colors(cID,:));
            frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 0;
            ttable{frame.cellsIDs(cellind),1} = colergen(cellstr(rgb2hex(colors(cID,:))),num2str(cID)); 
        else
            set(Hobj,'Color',[1,0,0]);
            frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 1;
            ttable{frame.cellsIDs(cellind),1} = colergen(cellstr(rgb2hex([1,0,0])),num2str(cID)); 
        end
        set(gui.tables(gui.handleMap.('TreeTable')),'Data',ttable);
% $$$         if(color(1)==1)
% $$$             set(Hobj,'Color',[0,1,0]);
% $$$             frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 0;
% $$$         elseif(color(2)==1)
% $$$             set(Hobj,'Color',[1,0,0]);
% $$$             frameset{nst,nxy,nch,nit}.selectedcells(cellind) = 1;
% $$$         end
    end
end